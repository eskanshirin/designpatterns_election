package election.business;

import java.time.LocalDate;

import election.business.DawsonElection;
import election.business.DawsonVoter;
import election.business.StubTally;
import election.business.interfaces.Election;
import election.business.interfaces.Voter;
import lib.PostalCode;

/**
 * @author 1631492 Lara
 *
 */
public class DawsonVoterTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		testTheFourParamConstructor();
		testTheThreeGetters();
		testTheSetter();
		testIsEligible();
		equalsTest();
		testHashCode();
		toStringTest();
		testCompareTo();
	}
	
	public static void testTheFourParamConstructor() {
		
			System.out.println("This will test the 4 param constructor");
			testTheFourParamConstructor("Case1-a succesful intantiation of an object from type DawsonVoter","Kiara","Molio","Kiki24@gmail.com","H8T2B1");
		
		}//end theFourParamCon.
	
	public static void testTheFourParamConstructor(String theCase,String fname,String lname,String email,String postalCode) {
		
			DawsonVoter dv= new DawsonVoter(fname,lname,email,postalCode);
			System.out.println(theCase+dv.toString()); 
			
		}//end theFourParamCon overload
		
	public static void testTheThreeGetters() {
		
			System.out.println("This will test the 3 getters");
			testTheThreeGetters("Case 1- Cleo Morin is expected to be returned  ","Cleo","Morin","cleo24@gmail.com","J6J3B7",1);
			testTheThreeGetters("Case 2- Cleo24@gmail.com is expected to be returned  ","Cleo","Morin","cleo24@gmail.com","J6J3B7",2);		
			testTheThreeGetters("Case 3- J6J3B7 is expected to be returned  ","Cleo","Morin","cleo24@gmail.com","J6J3B7",3);
	
		}//end theThreeGetters
		
	public static void testTheThreeGetters(String theCase,String fname,String lname, String email, String postalCode, int testNumber) {
		//number is used to the each case separately
			DawsonVoter dv = new DawsonVoter(fname,lname,email,postalCode);
	
				if(testNumber==1) {
				System.out.println(theCase + dv.getName() + " WORKS");
				}
				if(testNumber==2) {
				System.out.println(theCase + dv.getEmail() + " WORKS");
				}
				if(testNumber==3) {
				System.out.println(theCase + dv.getPostalCode() + " WORKS");
				}
				
		}//end theThreeGetters overload
		
	public static void testTheSetter() {
		
			System.out.println("This will test the setter");
			DawsonVoter dv= new DawsonVoter("Lara","Mosh","LaraMo1999@gmail.com","H8T2B1");
			PostalCode pc= new PostalCode("J6J3B7");
			System.out.println("This should change the postalCode from H8T2B1 ----> J6J3B7  The new code is " + dv.getPostalCode());
			
		} //end theSetter
		
	public  static void testIsEligible() {  
		
	System.out.print("This method is checking the isEligble()");	

	isEligible("Case 1-The election has not started yet , should return false----> ",1);
	isEligible("Case 2-The election has started , should return true, no range----->  ",2);
	isEligible("Case 3-The election has started , has range ,doesnt fit in it----->  ",3);
	isEligible("Case 4-The election has  started ,has a range , fits in it------>  ",4);

	}//end testIsEligible
	
	public static void  isEligible(String caseTest,int numberCase) { //number is used to the each case separately
		DawsonVoter dv = new DawsonVoter("Lara","Mo","laramo1999@gmail.com","H8T2B1"); 
		DawsonVoter dv1 = new DawsonVoter("Lara","Mo","laramo1999@gmail.com","H8BH8C"); 

	    DawsonElection de= new DawsonElection( "name","type", 
	    		 		2018, 04,23,
	    		 		2019,03,12,
	    		 		"","",new StubTally(),
    		 			"items");
	    
	    DawsonElection de1= new DawsonElection( "name","type", 
		 	   2016,04,23,
		 		2018,03,12,
		 		"","",new StubTally(),
	 			"items");
	    
	    DawsonElection de2= new DawsonElection( "name","type",  
			 	   2016, 04,23,
			 		2018,03,12,
			 		"H8B","H8D",new StubTally(),
		 			"items");
	    
	    
	    if(numberCase==1) {
	    	System.out.print(caseTest + dv.isEligible(de));
	       
	    }
	    
	    if(numberCase==2) {
	    	System.out.print(caseTest + dv.isEligible(de1));
	       
	    }
	    
	    if(numberCase==3) {
	    	System.out.print(caseTest + dv.isEligible(de2)); 
	       
	    }
	    
	    if(numberCase==4) {
	    	System.out.print(caseTest + dv1.isEligible(de2));
	       
	    }
	
	}//
	
	
     public static  void equalsTest() {
		   
	   equalsTest("Case1-VALID-it's the same obj-returns true",1);
	   equalsTest("Case2-INVALID-obj is null-returns false",2);
	   equalsTest("Case3-INVALID-its the same instance-returns false",3);
	   equalsTest("Case4-VALID-this and obj have the same email-returns true",4);
	   equalsTest("Case5-VALID-this and obj don't have the same email-returns false",5);
		 
     }//end equalsTest   
		   
    public static void equalsTest(String caseTest , int number) {//number is used to the each case separately
    	
		DawsonVoter dv1=new DawsonVoter("Jaya","Nila","Jaya123@gmail.com","H8T2B1");
		DawsonVoter dv2=null;
		DawsonVoter dv3=new DawsonVoter("Tricia","Camp","Tricia34@yahoo.co.il","J6J3B7");
		PostalCode cp =new PostalCode("H8T2B1");
		DawsonVoter dv4=new DawsonVoter("Moli","Erom","Jaya123@gmail.com","H8T2B1");

		
			if(number==1) {
			System.out.println(caseTest + "..... What is actually returns-->" + dv1.equals(dv1));
			}
		
			if(number==2) {
			System.out.println(caseTest + "...... What is actually returns-->" + dv1.equals(dv2));
			}
			
			if(number==3) {
			System.out.println(caseTest + "...... What is actually returns-->" + dv1.equals(cp));
			}
			
			if(number==4) {
			System.out.println(caseTest + "...... What is actually returns-->" + dv1.equals(dv4));
			}
			
			if(number==5) {
			System.out.println(caseTest + "...... What is actually returns-->" + dv1.equals(dv3));
			}
			
		 }//end equalsTestOverload
			  
	    
	    public static void testHashCode(){
	    	
	    	System.out.println("This method will test the hashCode");
	    	DawsonVoter dv= new DawsonVoter("Moran","Jojo","mjojo@yahoo.com","H8T2B1");
	    	DawsonVoter dv1= new DawsonVoter("Moran","Jojo","mjojo@yahoo.com","H8T2B1");
	    	System.out.println("This hashCode ----> " + dv.hashCode() + "<---should be the same as the hashcode of the other"
	    			+ " object that has the same email --->" + dv1.hashCode());
	    	
	    	}// end hashCode()
	
	    	
	 
	    public static  void toStringTest() {
	    	
	        	DawsonVoter dv = new DawsonVoter("Lior","Moragnoviz","Lior@fido.com","H8T2B1");
	    		System.out.println("This is a test for toString---->should println Lior@fido.com*"
	    							+ "Lior*Moragnoviz*H8T2B1" + "it printlns " + dv.toString());
	    	}//end toStringTest
	    	
	    	                                                                                      
	   	public static void testCompareTo() {
	   		
	    		System.out.println("This is to test the CompareTo method");	
	    		testCompareTo("Case1-will return 0 , equal",0);
	    		testCompareTo("Case2-will return a positive, since first smaller then second",1);
	    		testCompareTo("Case2-will return a negative, since first smaller then second",-1);
	    		
	    	    }//end compareTo
	    	
	    	
	    public  static void  testCompareTo(String caseTest,int expected) {
	    	
	    		DawsonVoter dv1= new DawsonVoter("Lili","Honos","sLiod@gmail.com","H8T2B1");
	    		DawsonVoter dv2= new DawsonVoter("Zili","Honos","azd@gmail.com","H8T2B1");
	    		
	    		if(expected==0) {
	    			System.out.println(caseTest + "-->" + dv1.compareTo(dv1));                                  
	    		}
	    		
	    		if(expected==1) {	
	    			System.out.println(caseTest + "-->" + dv1.compareTo(dv2)); 
	    		}
	    		
	        	if(expected==-1) {	
	        		System.out.println(caseTest + "-->" + dv2.compareTo(dv1)); 
	        	}
	        	
	    	}//end compareTo() overload
	
}// end DawsonVoter
	
		
		
		
