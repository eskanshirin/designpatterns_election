package election.business;

import election.business.DawsonBallotItem;
import election.business.DawsonVoter;

/**
 * This class is testing DawsonBallotItem method
 * @author Lara
 */


public class DawsonBallotItemTest {

	/**
	 * @param args
	 */
	
public static void main(String[] args) {
testTheTwoParamConstructor();
testTheCopyCon();
testTheSetter();
equalsTest();
testHashCode();
testCompareTo();
}

	public static void testTheTwoParamConstructor() {
		System.out.print("This will test the 2 param constructor");
		testTheTwoParamConstructor("Case1-a succesful intantiation of an object from type DawsonBallotItem","Candidate1",4,1);
		testTheTwoParamConstructor("Case2-Exception will be throwen, choice is null",null,4,2);
		testTheTwoParamConstructor("Case3-Eception will be throwen, choice is an empty string","",4,3);
		testTheTwoParamConstructor("Case4-Eception will be throwen, maxValue is a negative number","first",-35,4);

	
	}//end theFourParamCon.

	public static void testTheTwoParamConstructor(String caseTest,String choice, int maxValue,int number) { //int number is used that each test case will run separately 
	
    if(number==1) {
    	DawsonBallotItem dbi1 = new DawsonBallotItem(choice,maxValue);
    	System.out.println(caseTest);
    
    }//end if 1
    
    
    if(number==2) {
    
        try {
   		     DawsonBallotItem dbi2 = new DawsonBallotItem(choice,maxValue);
   		}
    
    	catch (IllegalArgumentException iae) {
               System.out.println(caseTest);	
               System.out.println(iae.getMessage());
   		}
     
    }//end if 2
     	
     if(number==3) {
       	    
           try {
               DawsonBallotItem dbi3 = new DawsonBallotItem(choice,maxValue);
           }
            
           catch (IllegalArgumentException iae) {
                 System.out.println(caseTest);	
                 System.out.println(iae.getMessage());
          } 
          
     }//end if 3
         	
      if(number==4) {
           	    
           try {
           	   DawsonBallotItem dbi4 = new DawsonBallotItem(choice,maxValue);
           }
            catch (IllegalArgumentException iae) {
                   System.out.println(caseTest);	
                   System.out.println(iae.getMessage());
                   } 
            
        }//end if 4
     
    }//end testTheTwoParamConstrctor() overload
    
	public static void testTheCopyCon() {
		System.out.println("This checks the copy constrcutor");
		testTheCopyCon("Case 1-a copy of an object was succesfully instansiated  ","first",5);
	}//end copyCon
 
    public static void testTheCopyCon(String caseTest,String choice,int maxValue) {
		DawsonBallotItem dbi = new DawsonBallotItem(choice,maxValue);
		System.out.println(" an object was succesfully created ");
	    DawsonBallotItem dbi1 = new DawsonBallotItem(dbi);
	    System.out.println(caseTest + dbi1.toString()); //note that toStirng is tested	 
 }//end copyCon overload
    

    
    
	public static void testTheSetter() {
		
	System.out.println("This method will test the setter");
	testTheSetter("Case1-fail, the new value is negative , will throw an exception",-34,1);
	testTheSetter("Case2-fail, the new value is bigger than maxValue , will throw an exception",46,2);
	testTheSetter("Case3-WORKS!",1,3);
	
	}	
	
	public static void testTheSetter(String caseTest,int newValue,int number) {
		
		DawsonBallotItem dbi = new DawsonBallotItem("first",5);
	if(number==1) {
			
		try {
		dbi.setValue(newValue);	
		}
		
		catch (IllegalArgumentException iae){	
		System.out.println(caseTest);
		iae.getMessage();
				
			}
	}//end if 1
		
	if(number==2) 
		{
		 try {
		 dbi.setValue(newValue);	
		 }
	 	
	 	 catch (IllegalArgumentException iae){
		 System.out.println(caseTest);
		 iae.getMessage();	
		 }
		
	 }//end if 2
		
		if(number==3)
		{
           System.out.println("This is the intial value: " + dbi.getValue());
           System.out.println("this is the new one ");
           dbi.setValue(newValue);
           System.out.println(dbi.getValue());	

           System.out.println(caseTest);
		}		
	
	} //end theSetter overload
	
	
 
 public static void equalsTest() {
	 
	 System.out.println("This method tests the equals method");
	   equalsTest("Case1-VALID-it's the same obj-returns true",1);
	   equalsTest("Case2-INVALID-obj is null-returns false",2);
	   equalsTest("Case3-INVALID-its the same instance-returns false",3);
	   equalsTest("Case4-VALID-this and obj have the same choice-returns true",4);
	   equalsTest("Case5-VALID-this and obj don't have the same choice-returns false",5);	 
	   
 }//end equals
 
	public static void equalsTest(String caseTest , int number) {//number is used to the each case separately
	    	
			DawsonBallotItem dbi1 =new DawsonBallotItem("first",4);
			DawsonBallotItem dbi2=null;
			DawsonBallotItem dbi3=new DawsonBallotItem("second",2);
			DawsonVoter dv= new DawsonVoter("Lars","Don","edLarsDon@gmail.com","h8t2b1");
			DawsonBallotItem dbi4=new DawsonBallotItem("second",2);

		
			
				if(number==1) {
				System.out.println(caseTest + "..... What is actually returns-->" + dbi1.equals(dbi1));
				}
			
				if(number==2) {
				System.out.println(caseTest + "...... What is actually returns-->" + dbi1.equals(dbi2));
				}
				
				if(number==3) {
				System.out.println(caseTest + "...... What is actually returns-->" + dbi1.equals(dv));
				}
				
				if(number==4) {
				System.out.println(caseTest + "...... What is actually returns-->" + dbi3.equals(dbi4));
				}
				
				if(number==5) {
				System.out.println(caseTest + "...... What is actually returns-->" + dbi1.equals(dbi4));
				}

	}//end equals overload
	
	
    	public static void testHashCode(){
    		
    	System.out.print("This method will test the hashCode");
    	DawsonBallotItem dbi1= new DawsonBallotItem("first",34);
    	DawsonBallotItem dbi2= new DawsonBallotItem("first",34);
    	System.out.println(dbi1.hashCode()+ "<---should be the same as the hashcode of the other"
    			+ " object that has the same choice --->"+ dbi2.hashCode());
    	
    	}// end hashCode()

    	
 
    
    	public static void testCompareTo() {
    		
    		System.out.print("This is to test the CompareTo method");	
    		testCompareTo("Case1-will return 0 , equal",0);
    		testCompareTo("Case2-will return a positive, since first smaller then second",1);
    		testCompareTo("Case2-will return a negative, since first smaller then second",-1);
    	    }//end compareTo
    	
    	
    	public  static void  testCompareTo(String caseTest,int expected) {
    		
    		DawsonBallotItem dbi1= new DawsonBallotItem("first",34);
        	DawsonBallotItem dbi2= new DawsonBallotItem("second",34);
        	DawsonBallotItem dbi3= new DawsonBallotItem("first",34);
        
        	
    		if(expected==0) {
    		System.out.print(caseTest + "-->" + dbi1.compareTo(dbi3));                                  
    		}
    		
    		if(expected==1) {	
        	System.out.print(caseTest + "-->" + dbi2.compareTo(dbi1)); 
    		}
    		
        	if(expected==-1) {	
            System.out.print(caseTest + "-->" + dbi1.compareTo(dbi2)); 
        	}
        	
    	}//end compareTo() overload





}//end dawsonBallotItemTest


