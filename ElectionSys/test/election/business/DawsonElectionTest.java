/**
 * 
 */
package election.business;


import election.business.interfaces.*;
import lib.PostalCode;


/**
 * @author Farzaneh Sabzehali & Shirin Eskandari
 * @version 28 September 2017
 *
 */
public class DawsonElectionTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		testConstructor();
		testIsLimitedToPostalRange();
		testEquals();
		testHashCode();
		testSetTally();
		testCompareTo();

	}
	
	public static void testConstructor() {
		StubTally stubTally = new StubTally();
		System.out.println("\t\t\n===== Constructor() TEST CASES ===== ");
		testConstructor("Case 1: Null name", null, "single", 2017, 10, 5, 2017, 11, 5,
						null, null, stubTally, false,"Mcgil MBA group", "Brittany independence referendum");
		testConstructor("Case 2: Invaid Date", "electoin1", "single",  2017, 13, 5, 2017, 11, 5,
				null, null, stubTally,false,"Mcgil MBA group", "Brittany independence referendum");
		testConstructor("Case 3: Start date after end date", "electoin1", "single",  2017, 12, 5, 2017, 11, 5,
				null, null, stubTally,false,"Mcgil MBA group", "Brittany independence referendum");
		testConstructor("Case 4: Ballot items less than 2", "electoin1", "single",  2017, 10, 5, 2017, 11, 5,
				null, null, stubTally,false,"Mcgil MBA group");
		testConstructor("Case 5: Valid parameters ", "Election1", "single", 2017, 10, 5, 2017, 11, 5,
				null, null, stubTally, false,"Mcgil MBA group", "Brittany independence referendum");
	}
	
	public static void testConstructor(String testCase, String name, String type, 
			int startYear, int startMonth, int startDay,
			int endYear, int endMonth, int endDay,
			String startRange, String endRange, StubTally stubTally,
			boolean expectValid,String... items ) {
		
		System.out.println("   " + testCase);
		try {
			DawsonElection  test = new DawsonElection (name, type, startYear, startMonth, startDay, endYear, endMonth, endDay,
								null, null, stubTally,items );
			System.out.println("\tPostalCode instance is created: " + test);
		}
		catch(IllegalArgumentException iae) {
			
			if(!expectValid)
			System.out.println("Passed Test");
		}
		catch (Exception e) {
			System.out.println("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() +  " "  + 	e.getMessage() + " ==== FAILED TEST ====");
			if (expectValid)
				System.out.print(" Expected Valid.");
		}
		
	}
	
	public static void testIsLimitedToPostalRange() {
		StubTally stubTally = new StubTally();
		System.out.println("\t\t\n===== isLimitedToPostalRange() TEST CASES ===== ");
		testIsLimitedToPostalRange("Case 1: Null startRange and or endRange", "Election1", "single", 2017, 10, 5, 2017, 11, 5,
			null, null, stubTally, false,"Mcgil MBA group", "Brittany independence referendum");
		testIsLimitedToPostalRange("Case 1: Valid start and end range", "Election1", "single", 2017, 10, 5, 2017, 11, 5,
				"h", "j", stubTally, true,"Mcgil MBA group", "Brittany independence referendum");
	}
	
	public static void testIsLimitedToPostalRange(String testCase, String name, String type, 
			int startYear, int startMonth, int startDay,
			int endYear, int endMonth, int endDay,
			String startRange, String endRange, StubTally stubTally,
			boolean expectValid,String... items) {
		
		System.out.println("   " + testCase);
		DawsonElection  test = new DawsonElection (name, type, startYear, startMonth, startDay, endYear, endMonth, endDay,
				null, null, stubTally,"Mcgil MBA group", "Brittany independence referendum" );
		if((startRange ==null || endRange == null) && !expectValid)
			System.out.println("Passed Test");
		else if (expectValid)
			System.out.println("Passed Test");
		
	}
	
	public static void testEquals() {
		StubTally stubTally = new StubTally();
		System.out.println("\t\t\n===== equals() TEST CASES ===== ");
		testEquals("Case 1: Same name and same class", "Election1","Election1", "single", 2017, 10, 5, 2017, 11, 5,
				null, null, stubTally, true,"Mcgil MBA group", "Brittany independence referendum");
		testEquals("Case 2: Different name","Election1", "Election2", "single", 2017, 10, 5, 2017, 11, 5,
				null, null, stubTally, false,"Mcgil MBA group", "Brittany independence referendum");
		
	}	
	
	public static void testEquals(String testCase, String name1, String name2, String type, 
			int startYear, int startMonth, int startDay,
			int endYear, int endMonth, int endDay,
			String startRange, String endRange, StubTally stubTally,
			boolean expectValid,String... items) {
		
		DawsonElection  test1 = new DawsonElection (name1, type, startYear, startMonth, startDay, endYear, endMonth, endDay,
				null, null, stubTally,items );
		DawsonElection  test2 = new DawsonElection (name2, type, startYear, startMonth, startDay, endYear, endMonth, endDay,
				null, null, stubTally,items );
		if ((test1.getName().equals(test2.getName())) && test2 instanceof DawsonElection )
			System.out.println(testCase + "\nPassed Test");
		else if (!(test1.getName().equals(test2.getName())) && test2 instanceof DawsonElection )
			System.out.println(testCase + "\nPassed Test");
		else
			System.out.println("Error! Unexpected Result. ==== Faild Test ==== ");
				
	}
	
private static void testHashCode() {
	
		StubTally stubTally = new StubTally();
		System.out.println("\t\t===== hashCode() TEST CASES ===== ");
		testHashCode("Case 1: Same name and same class", "Election1","Election1", "single", 2017, 10, 5, 2017, 11, 5,
				null, null, stubTally, true,"Mcgil MBA group", "Brittany independence referendum");
		testHashCode("Case 2: Different name","Election1", "Election2", "single", 2017, 10, 5, 2017, 11, 5,
				null, null, stubTally, false,"Mcgil MBA group", "Brittany independence referendum");
		
	}
	
	private static void testHashCode(String testCase, String name1, String name2, String type, 
			int startYear, int startMonth, int startDay,
			int endYear, int endMonth, int endDay,
			String startRange, String endRange, StubTally stubTally,
			boolean expectValid,String... items) {
		
		System.out.println("   " + testCase);
		try {
			DawsonElection  test1 = new DawsonElection (name1, type, startYear, startMonth, startDay, endYear, endMonth, endDay,
					null, null, stubTally,items );
			DawsonElection  test2 = new DawsonElection (name2, type, startYear, startMonth, startDay, endYear, endMonth, endDay,
					null, null, stubTally,items );
			if(expectValid && (test1.hashCode() == test2.hashCode()));
			System.out.println("Passed Test");
		}
		catch(IllegalArgumentException iae) {
			
			System.out.println("Passed Test");
		}
		catch (Exception e) {
			System.out.println("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() +  " "  + 	e.getMessage() + " ==== FAILED TEST ====");
			if (expectValid)
				System.out.print(" Expected Valid.");
		}
	}
	
	
	public static void testSetTally() {
		
		Tally tally = new StubTally();
		DawsonElection de = new DawsonElection("Nasrin", "single", 1981, 11, 25, 
				1981, 12, 25, "H", "P", tally, "Shirin","Shadi", "Farshid");
	
		testSetTally(tally, de);
	}
	
	public static void testSetTally(Tally tally, DawsonElection de) {
	
		try {
			de.setTally(tally);
		}
		catch(IllegalArgumentException iae) {
			System.out.print("\nUNEXPECTED EXCEPTION TYPE! " + iae.getClass() + " " +
			     iae.getMessage() + " ==== FAILED TEST ====");
		}
	}
	
	public static void testCompareTo() {
		   System.out.println("\n\nTesting the compareTo method.");
		   testCompareTo("Case 1: will return 0 , they are equal","Nasrin",0);
		   testCompareTo("\nCase 2: will return negative , they are not equal false","Nasrin",1);
		   testCompareTo("\nCase 3: will return positive , they are notequal false","Nasrin",-1);
	}
	public  static void  testCompareTo(String testCase,String name,int expected) {
		System.out.println("    " + testCase);
		DawsonElection dee = new DawsonElection(name, "single", 1981, 11, 25, 
				1981, 12, 25, "H", "P", new StubTally(), "Shirin","Shadi", "shir");
		
		DawsonElection de = new DawsonElection(name ,"single", 2017, 11, 25, 
				2017, 12, 25, "H", "P", new StubTally(), "Shirin","Shadi", "shir");
		DawsonElection dee2 = new DawsonElection("Nasriin" ,"single", 2017, 11, 25, 
				2017, 12, 25, "H", "P", new StubTally(), "Shirin","Shadi", "shir");
		DawsonElection dee3 = new DawsonElection("Nasrn" ,"single", 2017, 11, 25, 
				2017, 12, 25, "H", "P", new StubTally(), "Shirin","Shadi", "shir");
		if(expected==0) {
		System.out.print("expected result=0 and  the result = " + de.getName().compareTo(dee.getName())); 
		}
		
		if(expected==1) {	
  	System.out.print("expected result=negative  and the result =  " + de.getName().compareTo(dee3.getName())); 
		}
  	if(expected==-1) {	
      	System.out.print("expected result= positive and the result =  " + de.getName().compareTo(dee2.getName())); 
  	}
	}//end compareTo()
	public static void testGetElectionChoices( DawsonElection dee) {
		System.out.println(dee.getElectionChoices().toString());
	}

}