package election.data;

import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;


import election.business.DawsonTally;
import election.business.interfaces.Election;
import election.business.interfaces.Tally;
import util.ListUtilities;


public class ElectionFileLoaderTest {
	
	public static void main(String[] args) throws IOException
	{
		
	testGetFilesName();
	testGetVoterList();
	testSetExistingTallyFromFile();

	
		
	}	
	public static void testGetFilesName()
		{
			System.out.println("Testing for loading election.txt files in the path" );
			testGetFilesName("case1: invalid data, empty election file","datafiles/unsorted",false);
			testGetFilesName("case2: invalid data, wrong name for  election file","datafiles/unsorted/election.txt",false);
			testGetFilesName("case3: invalid data, invalid data start date in  election file","datafiles/unsorted/elections8.txt",false);
			testGetFilesName("case4: valid data, correct election file","datafiles/unsorted/elections4.txt",true);
		}
		
		public static void testGetFilesName(String testCase, String filename, boolean expectValid)
		{
			 System.out.println("   "+testCase);
			 try{
				 ElectionFileLoader.getElectionListFromSequentialFile(filename) ;
				
			           
			           if (!expectValid)
			                System.out.println(" Error! EXPECTED INVALID. == FAILED TEST=="  );
			              System.out.println("\n");
			      }
			      catch(IllegalArgumentException iae){
			        System.out.print("\t"+ iae.getMessage());
			   if (expectValid)
			    System.out.print("  Error! Expected Valid. ==== FAILED TEST ====");
			  }
			  catch (Exception e) {
			   System.out.print("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() + " " +
			     e.getMessage() + " ==== FAILED TEST ====");
			   if (expectValid)
			    System.out.print(" Expected Valid.");
			  }

			  System.out.println("\n");
			 
			 }
		private static void testGetVoterList() {
			System.out.println("\t\t===== getVotersList TEST CASES ===== ");
			testGetVoterList("Case 1: Invalid file", "voter.txt", false);
			testGetVoterList("Case 2: empty file", "voterEmpty", false);
			testGetVoterList("Case 3: Valid file", "Voters6.txt", true);
			testGetVoterList("Case 4: Invalid data", "Voters4.txt", false);
			
		}
		
		private static void testGetVoterList(String testCase, String filename, boolean expected) {
			
			System.out.println("   " + testCase);
			
			try {
				ElectionFileLoader.getVoterListFromSequentialFile(filename);
				
				System.out.println("Test Pass.");
			}// end try
			
			catch (IllegalArgumentException iae) {
				if(!expected)
					System.out.println("Test Pass.");
				else
					System.out.println("Test Fail=== "+iae.getMessage());
			}//end catch iae
			
			catch(IOException ioe) {
				if(!expected)
					System.out.println("Test Pass.");
				else
					System.out.println("Test Fail==="+ioe.getMessage());
				
			}//end catch ioe
		}
		
		public static void testSetExistingTallyFromFile() {
			System.out.println("\t\t===== setExistingTallyFromFile TEST CASES ===== ");
			
			testSetExistingTallyFromFile("Case 1: Valid data", "datafiles/unsorted/tally.txt", true);
		}
		
		
		public static void testSetExistingTallyFromFile(String testCase, String filename, boolean expected) {
			System.out.println("   " + testCase);
			
			
			try {
				
				Election[] elections = ElectionFileLoader.getElectionListFromSequentialFile("datafiles/unsorted/elections6.txt");
				ElectionFileLoader.setExistingTallyFromSequentialFile(filename, elections);
				System.out.println("Test Pass");
			}//end try
			
			catch (IllegalArgumentException iae) {
				if(!expected)
					System.out.println("Test Pass.");
				else
					System.out.println("Test Fail==="+iae.getMessage());
			}//end catch iae
			
			catch (NullPointerException npe) {
				System.err.println(npe.getMessage());
			}
			catch (IOException ioe) {
				if(!expected)
					System.out.println("Test Pass.");
				else
					System.out.println("Test Fail==="+ioe.getMessage());
				}
			}//end catch ioe
}
