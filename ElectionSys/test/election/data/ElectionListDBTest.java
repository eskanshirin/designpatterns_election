package election.data;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import election.business.DawsonElection;
import election.business.DawsonElectionFactory;
import election.business.interfaces.Election;
import election.business.interfaces.ElectionFactory;
import election.business.StubTally;
import election.business.interfaces.Tally;
import election.business.interfaces.Voter;
import util.ListUtilities;
import util.Utilities;
import election.data.DuplicateElectionException;

/**
 * @author Sonya, Shirin
 *
 */
public class ElectionListDBTest {

	public static void main(String[] args) {
		
		testDisconnect();
		testConstructor();
		
		testGetElection();
		testAdd();
		
		
		
	}
	
	private static void setup()
	{
		String[] voters = new String[2];
		voters[0] = "joe.mancini@mail.me*Joe*Mancini*H3C4B7";
		voters[1] = "raj@test.ru*Raj*Wong*H3E1B4";
		//TODO add more voters if needed, but they must be in sorted order!

		String[] elecs = new String[3];
		elecs [0] = "Brittany independence referendum*2017*9*6*2018*8*5*H3A*M1Z*single*2" +
				"\nYes, I want independence" +
				"\nNo, I do not want independence";
		elecs [1] = "Favourite program*2018*5*1*2019*5*31*H4G*H4G*single*2" + 
				"\nGame of Thrones" + 
				"\nNarcos";
		elecs [2] = "Presidental race*2020*11*1*2020*11*1***single*2" + 
				"\nDonald Trump" + 
				"\nAnyone Else";
		//TODO add more elections if needed, but the must be in sorted order

		String[] tallies = new String[2];
		tallies [0] = "Presidental race*2" + 
				"\n100*0" + 
				"\n0*102";
		tallies [1] = "Favourite program*2" + 
				"\n1000*0" + 
				"\n0*560";
		
		// make the testfiles directory
		Path dir;
		try {
			dir= Paths.get("datafiles/testfiles");
			if( !Files.exists(dir))
				Files.createDirectory(dir);
			ListUtilities.saveListToTextFile(voters, 
					"datafiles/testfiles/testVoters.txt");
			ListUtilities.saveListToTextFile(elecs, 
					"datafiles/testfiles/testElections.txt");
			ListUtilities.saveListToTextFile(tallies, 
					"datafiles/testfiles/testTally.txt");
			//creates object serialized files from the sequential text files
			//reads the sequential test files and serialize the lists.
			SequentialTextFileList lists = new SequentialTextFileList("datafiles/testfiles/testVoters.txt",
					"datafiles/testfiles/testElections.txt", "datafiles/testfiles/testTally.txt");
			Utilities.serializeObject(lists.getElectionDatabase(), "datafiles/testfiles/testElections.ser");
			
		} catch (InvalidPathException e) {
			System.err.println("could not create testfiles directory " + e.getMessage());
		} catch (FileAlreadyExistsException e) {
			System.err.println("could not create testfiles directory " + e.getMessage());
		} catch (IOException e) {
			System.err.println("could not create testfiles in setup() " + e.getMessage());
		}

	}	
	
	private static void teardown() {
		Path file;
		try {
			file = Paths.get("datafiles/testfiles/testVoters.txt");
			Files.deleteIfExists(file); 
			file = Paths.get("datafiles/testfiles/testElections.txt");
			Files.deleteIfExists(file); 
			file = Paths.get("datafiles/testfiles/testTally.txt");
			Files.deleteIfExists(file); 
		} catch (InvalidPathException e) {
			System.err.println("could not delete test files " + e.getMessage());
		} catch (NoSuchFileException e) { 
			System.err.println("could not delete test files " + e.getMessage());
		} catch (DirectoryNotEmptyException e) {
			System.err.println("could not delete test files " + e.getMessage());
		 } catch (IOException e) { 
		    System.err.println("could not delete test files " + e.getMessage());
		} 
		
	}
	
	public static void testConstructor () {
		setup();
		
		System.out.println("Testing the ElectionListDB constructor");
		
		SequentialTextFileList file = new SequentialTextFileList
		("datafiles/testfiles/testVoters.txt", "datafiles/testfiles/testElections.txt",
				"datafiles/testfiles/testTally.txt");
		DawsonElectionFactory factory = DawsonElectionFactory.DAWSON_ELECTION;

		testOneParamConstructor("Giving a ListPersistenceObject that is valid", file);
		testOneParamConstructor("Giving a ListPersistenceObject that is null", null);
		
		testTwoParamConstructor("Giving a ListPersistenceObject and factory that are valid", file, factory);
		testTwoParamConstructor("Giving a ListPersistenceObject that is null", null, factory);
		testTwoParamConstructor("Giving a factory that is null", file, null);
		
		teardown();
	}// End testConstructor
	
	public static void testOneParamConstructor (String testCase, SequentialTextFileList file) {
		try {
			ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
					"datafiles/testfiles/testElection.ser");
			ElectionListDB electionDB = new ElectionListDB(list);
			
			System.out.println("Testing the One-parameter constructor");
			
			System.out.println(electionDB.toString());
		} catch (IllegalArgumentException iae) {
			
			System.err.println(iae.getMessage());
		}
	}// End testOneParamConstructor method
	
	public static void testTwoParamConstructor (String testCase, SequentialTextFileList file, ElectionFactory factory) {
		try {
			ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
					"datafiles/testfiles/testElection.ser");
			ElectionListDB electionDB = new ElectionListDB(list, factory);
			
			System.out.println("Testing the Two-parameter constructor");
			
			System.out.println(electionDB.toString());
		} catch (IllegalArgumentException iae) {
			
			System.err.println(iae.getMessage());
		}
	}// End testTwoParamConstructor method
	
	
	private static void testAdd() {
		setup();
		
		//SequentialTextFileList file = new SequentialTextFileList
				//("datafiles/testfiles/testVoters.txt", "datafiles/testfiles/testElections.txt",
					//	"datafiles/testfiles/testTally.txt");
		ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
				"datafiles/testfiles/testElection.ser");
		ElectionListDB electionDB = new ElectionListDB(list);
		
		System.out.println("\n** test ElectionListDB add() method ** ");
		
		//DawsonElection object that is in the database
		Election duplicateElection = DawsonElectionFactory.DAWSON_ELECTION.getElectionInstance("Favourite program", "single", 
				2018, 5, 1, 2019, 5, 31,
				"H4G", "H4G",
				"Game of Thrones", "Narcos");
		
		Election newElection = DawsonElectionFactory.DAWSON_ELECTION.getElectionInstance("Meal to eat for the rest of the day", "ranked", 
				2017, 12, 1, 2019, 5, 31,
				"H1H", "H7N",
				"Fatteh", "Vereniki", "Shnitzel");

		testAdd("Add an Election that EXISTS in the database ALREADY", electionDB, duplicateElection);
		testAdd("Add a NEW Election to the database", electionDB, newElection);
		testAdd("Add a NULL Election object to the database", electionDB, null);
		
		teardown();
	}// End testAdd method
	
	private static void testAdd (String testCase, ElectionListDB electionDB, Election election) {
		try {
			System.out.println(testCase);
			
			electionDB.add(election);
			System.out.println(electionDB.toString());
			System.out.println("TestPassed! Election added");
		} catch (DuplicateElectionException dee) {
			
			System.err.println(dee.getMessage());
		} catch (IllegalArgumentException iae) {
			
			System.err.println(iae.getMessage());
		} 
	}// End testAdd method
private static void testGetElection()  {
		
		setup();
		//SequentialTextFileList file = new SequentialTextFileList
				//("datafiles/testfiles/testVoters.txt", "datafiles/testfiles/testElections.txt",
					//	"datafiles/testfiles/testTally.txt");
		ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
				"datafiles/testfiles/testElection.ser");
		ElectionListDB eldb = new ElectionListDB(list);
		
		System.out.println("\n** test getElection ** ");
		System.out.println("\nTest case 1: election in database:");
		try {
			Election election = eldb.getElection("Brittany independence referendum");
			System.out.println("SUCCESS: Election found " + election.toString());
		} catch (InexistentElectionException e) {
			System.out.println("FAILING TEST CASE: election should be fould" + e.getMessage());
		}
		
		System.out.println("\nTest case 2: Election not in database:");
		try {
			Election election = eldb.getElection("shirin");
			System.out.println("FAILING TEST CASE: Voter found " + election.toString());
		} catch (InexistentElectionException e) {
			System.out.println("SUCCESS: election not found");
		}	

		teardown();
	}


	
	private static void testDisconnect() 
	{
		setup();
		//SequentialTextFileList file = new SequentialTextFileList
				//("datafiles/testfiles/testVoters.txt", "datafiles/testfiles/testElections.txt",
					//	"datafiles/testfiles/testTally.txt");
		ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
				"datafiles/testfiles/testElection.ser");
		ElectionListDB eldb = new ElectionListDB(list);
		
		System.out.println("\n** test disconnect() ** ");
		try {

		eldb.toString();
		eldb.disconnect();
		eldb.toString();
		
		}catch(NullPointerException npe) 
		{
		 System.out.println("Data base is null "+ npe.getMessage() );
	     }
		
		catch(IOException ioe)  {
			System.out.println(ioe.getMessage());
			System.out.println("SUCCESS: election not found");
		}	
	teardown();	
		}

	
		

}// End ElectionListDBTest class