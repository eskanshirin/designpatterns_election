package election.data;

/**
 * @author (Jaya,Maja)Lara
 */
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import election.business.DawsonElectionFactory;
import election.business.interfaces.*;
import lib.*;
import util.ListUtilities;
import util.Utilities;

public class VoterListDBTest {

	public static void main(String[] args) {
		testGetVoter();
		testAdd();
		testDisconnect();
		testUpdate();


	}

	private static void setup() {
		String[] voters = new String[3];
		voters[0] = "joe.mancini@mail.me*Joe*Mancini*H3C4B7";
		voters[1] = "raj@test.ru*Raj*Wong*H3E1B4";
		voters[2] = "lirt@uloo.ca*Chin*Wa*H8T2B1";

		String[] voters1 = new String[1];
		voters1[0] = null;

		String[] elecs = new String[2];
		elecs[0] = "Presidental race*2020*11*1*2020*11*1***single*2" + "\nDonald Trump" + "\nAnyone Else";
		elecs[1] = "Favourite program*2018*5*1*2019*5*31*H4G*H4G*single*2" + "\nGame of Thrones" + "\nNarcos";
		// TODO add more elections if needed, but the must be in sorted order

		String[] tallies = new String[2];
		tallies[0] = "Presidental race*2" + "\n100*0" + "\n0*102";
		tallies[1] = "Favourite program*2" + "\n1000*0" + "\n0*560";

		// make the test files directory
		Path dir;
		try {
			dir = Paths.get("datafiles/testfiles");
			if (!Files.exists(dir))
				Files.createDirectory(dir);
			ListUtilities.saveListToTextFile(voters, "datafiles/testfiles/testVoters.txt");
			ListUtilities.saveListToTextFile(elecs, "datafiles/testfiles/testElections.txt");
			ListUtilities.saveListToTextFile(tallies, "datafiles/testfiles/testTally.txt");
			//creates object serialized files from the sequential text files
			//reads the sequential test files and serialize the lists.
			SequentialTextFileList lists = new SequentialTextFileList("datafiles/testfiles/testVoters.txt",
					"datafiles/testfiles/testElections.txt", "datafiles/testfiles/testTally.txt");
			Utilities.serializeObject(lists.getVoterDatabase(), "datafiles/testfiles/testVoters.ser");
			
			
		} catch (InvalidPathException e) {
			System.err.println("could not create testfiles directory " + e.getMessage());
		} catch (FileAlreadyExistsException e) {
			System.err.println("could not create testfiles directory " + e.getMessage());
		} catch (IOException e) {
			System.err.println("could not create testfiles in setup() " + e.getMessage());
		}

	}

	private static void teardown() {
		Path file;
		try {
			file = Paths.get("datafiles/testfiles/testVoters.txt");
			Files.deleteIfExists(file);
			file = Paths.get("datafiles/testfiles/testElections.txt");
			Files.deleteIfExists(file);
			file = Paths.get("datafiles/testfiles/testTally.txt");
			Files.deleteIfExists(file);
		} catch (InvalidPathException e) {
			System.err.println("could not delete test files " + e.getMessage());
		} catch (NoSuchFileException e) {
			System.err.println("could not delete test files " + e.getMessage());
		} catch (DirectoryNotEmptyException e) {
			System.err.println("could not delete test files " + e.getMessage());
		} catch (IOException e) {
			System.err.println("could not delete test files " + e.getMessage());
		}
		

	}
	

	private static void testGetVoter() {
		System.out.println("--------------------------------------------------------------------------------------------------");
		setup();
		//SequentialTextFileList file = new SequentialTextFileList("datafiles/testfiles/testVoters.txt",
			//	"datafiles/testfiles/testElections.txt", "datafiles/testfiles/testTally.txt");
		ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
				"datafiles/testfiles/testElection.ser");
		VoterListDB db = new VoterListDB(list);

		System.out.println("\n ************** test getVoter ************** ");
		System.out.println("\nTEST CASE 1: Voter in database:");
		try {
			Voter voter = db.getVoter("raj@test.ru");
			System.out.println("SUCCESS: Voter found " + voter.toString());
		} catch (InexistentVoterException e) {
			System.out.println("FAILING TEST CASE: voter should be fould");
		}

		System.out.println("\nTEST CASE 2: Voter not in database:");
		try {
			Voter voter = db.getVoter("jar@test.ru");
			System.out.println("FAILING TEST CASE: Voter found " + voter.toString());
		} catch (InexistentVoterException e) {
			System.out.println("SUCCESS: voter not found");
		}

		teardown();
		System.out.println("--------------------------------------------------------------------------------------------------");

	}

	public static void testAdd() {

		setup();
		//SequentialTextFileList file = new SequentialTextFileList("datafiles/testfiles/testVoters.txt",
				//"datafiles/testfiles/testElections.txt", "datafiles/testfiles/testTally.txt");
		ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
				"datafiles/testfiles/testElection.ser");
		VoterListDB db = new VoterListDB(list); // the con works

		System.out.println("\n************** test addVoter ************** ");
		DawsonElectionFactory factory = DawsonElectionFactory.DAWSON_ELECTION;
		Voter v1 = factory.getVoterInstance("Lara", "Mo", "laramo1999@gmail.com", "H8T2B1"); // in database
		Voter v2 = null; // null
		Voter v3 = factory.getVoterInstance("Bob", "Mancini", "joe.mancini@mail.me", "H3C4B7"); // already exists

		try {
			System.out.println("\nTEST CASE 1: Voter is in database:");
        	db.add(v3); 
		}

		catch (DuplicateVoterException dve) 
		{
			 System.out.println("FAILING TEST CASE: Voter JOE found by email:" + dve.getMessage());
		}

		try
		{
		    System.out.println("\nTEST CASE 2: Voter is null ");
			db.add(v2);
		}
		catch (DuplicateVoterException e) 
		{
			System.out.println("THIS LINE OF CODE IS NOT SUPPOSED TO RUN");
		}
		catch (IllegalArgumentException iae) {
			System.out.println("FAILING TEST CASE: " + iae.getMessage());
		}


		try 
		{
			System.out.println("\nTEST CASE 3: Voter is not in database");
			db.add(v1);
			System.out.println("PASS TEST CASE: Voter was added (now 4 voters) : " + db.toString());

		}
		
		catch (DuplicateVoterException e) 
		{
			System.out.println(e.getMessage());
		}
		

		teardown();
		System.out.println("--------------------------------------------------------------------------------------------------");

	}

	public static void testUpdate() {

		setup();
		//SequentialTextFileList file = new SequentialTextFileList("datafiles/testfiles/testVoters.txt",
				//"datafiles/testfiles/testElections.txt", "datafiles/testfiles/testTally.txt");
		ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
				"datafiles/testfiles/testElection.ser");
		VoterListDB db = new VoterListDB(list);

		System.out.println("\n************** test UpdateVoter ************** ");
		
		Email email1 = new Email("joe.mancini@mail.me");// existing email
		Email email2 = new Email("Yolo@gmail.com"); // non existing
		Email email3 = null; //null
		PostalCode ps2 = null; //null
		PostalCode ps1 = new PostalCode("h8t2b1");// the one to update
		PostalCode ps3 = new PostalCode("J6J7B7");// the one to update

		
		try
		{
			System.out.println("\nTEST CASE 1:Email was found , voter is in the datebase");
			System.out.println("Voter 1 aka Joe, postalCode was: " + db.toString());
			db.update(email1, ps1);
			System.out.println("------>SUCESS, the first voter, Joe, UPDATED " + db.toString());
		}
		catch (InexistentVoterException ive) 
		{
			System.out.println(ive.getMessage());
		}
		
		
		
		System.out.println("\nTEST CASE 2: EMAIL IS NULL");
		try 
		{
			db.update(email3, ps1);
		} catch (IllegalArgumentException iae) 
		{
			System.out.println("FAIL: email is null ----> " + iae.getMessage());
		} catch (InexistentVoterException ive) {
			System.out.println(ive.getMessage());
		}
		
		
		System.out.println("\nTEST CASE 3: postalCode is null");
        try 
        {
			db.update(email1, ps2);
		} catch (IllegalArgumentException iae) 
        {
			System.out.println("FAIL:postalCode is null ---->  " + iae.getMessage());
		} catch (InexistentVoterException abc) 
        {
			System.out.println(abc.getMessage());
		}

		System.out.println("\nTEST CASE 4: Voter is not in database");
		try 
		{
			System.out.println("How it looks before the update \n\n" + db.toString());
            db.update(email2, ps3);
		} 
		catch (InexistentVoterException c) 
		{
			System.out.println("FAIL----->The voter was not updated to PostalCode J6J3B7  because yolo@gmail.com is not in database---->  "+ c.getMessage()+"\n"
					+
			db.toString());
		}

		teardown();
		System.out.println("--------------------------------------------------------------------------------------------------");

	}// end method

	private static void testDisconnect() {

		setup();
		//SequentialTextFileList file = new SequentialTextFileList("datafiles/testfiles/testVoters.txt",
				//"datafiles/testfiles/testElections.txt", "datafiles/testfiles/testTally.txt");
		ObjectSerializedList list = new ObjectSerializedList("datafiles/testfiles/testVoters.ser",
				"datafiles/testfiles/testElection.ser");
		VoterListDB db = new VoterListDB(list);

		System.out.println("\n************** test disconnect ************** ");
		
		try 
		{
			db.toString();
			db.disconnect();
			db.toString(); //will become null 
		}
   
	catch(NullPointerException npe) 
	{
	 System.out.println("FAIL----> null pointer exception would be throwen.Data base is null meaning, WE DISCONNECTED!!!");
     }
		catch(IOException oie) {
			System.out.println(oie.getMessage());
			System.out.print("success, voterdb was not found");
		}
		teardown();
		System.out.println("--------------------------------------------------------------------------------------------------");

	}
}// end class
