
package election.ui.gui;

import election.business.ElectionType;
import election.business.interfaces.*;
import election.data.InexistentVoterException;
import javafx.event.ActionEvent;
import javafx.geometry.*;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.*;

/** Form that gets the voter email, finds the Voter in the model
  * If the voter is eligible for the election, instantiate and start
  * a SingleBallotFormGUI
  */
public class VoterEmailFormGUI {
	
	private ElectionOffice model;
	private Election election;
    private TextField emailTextField; //text input
    private Text actionTarget; //label display
	/** 
	 * Constructor validates that the parameters are not null 
	 * and the election has ElectionType.SINGLE. Invokes the
	 * initialize() method
	 *  @throws IllegalArgumentException if the conditions are not met.
	 */
    public VoterEmailFormGUI(ElectionOffice model, Election election) {
		//validate the election argument
    	if(election==null) 
    	{
    		throw new IllegalArgumentException("Election can't be null");
    	}
    	
    	if(!election.getElectionType().equals(ElectionType.SINGLE))
    	{
    		throw new IllegalArgumentException("Election has to be null");
    	}
    	
    	this.model=model;
    	this.election=election;
    	//added by Farzaneh
    	 emailTextField = new TextField();
    	 actionTarget = new Text();
    	 //end
    }

    /**
     * The stage and the scene are created in the start.
     *
     * @param primaryStage
     */
    public void start(Stage primaryStage) {
    	// Set Window's Title
        primaryStage.setTitle("Get Voter Email");
        GridPane root = createUserInterface();
        Scene scene = new Scene(root, 500, 275);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Create the user interface as the root
     *
     * @return GridPane with the UI
     */
    private GridPane createUserInterface() {
/**
 * This form asks for the email(done)
 */
    	 GridPane grid = new GridPane();
         grid.setAlignment(Pos.CENTER);
         grid.setHgap(10);
         grid.setVgap(10);
         grid.setPadding(new Insets(25, 25, 25, 25)); 
         
         Text scenetitle = new Text("Welcome");
         scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
         grid.add(scenetitle, 0, 0, 2, 1);

         Label email = new Label("Enter email address:");
         grid.add(email, 0, 1);

         grid.add(emailTextField, 1, 1);

         Button btn = new Button("Sign in");
         HBox hbBtn = new HBox(10);
         hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
         hbBtn.getChildren().add(btn);
         grid.add(hbBtn, 1, 4);

         
         actionTarget.setId("actiontarget");
         grid.add(actionTarget, 0, 6, 2, 1);

         
         
         //event handler

         btn.setOnAction(this::signInButtonHandler);

         return grid;   
         }

    /**
     * Event handler for the Sign In Button
     *
     * @param e
     */
    private void signInButtonHandler(ActionEvent e) {
    	//checks that the voter is registered and eligible
    	Voter v=null; //assuming later instantiation
    	 try 
    	 {
<<<<<<< HEAD
    		  v=model.findVoter(emailTextField.getText());
=======
		 //changed by Farzaneh
    		  v=model.findVoter(emailTextField.getText());
		 //end changes
>>>>>>> 3a7f625f4db917a31306911892222582569f835e
    		 
    	 }
    	 
    	 catch(InexistentVoterException ive)
    	 {
    		 System.out.print(ive.getMessage());
    	 }
    	 

    	    //if still here , gets a ballot from the model
         Ballot b = model.getBallot(v,this.election);
         SingleBallotFormGUI sbfg = new SingleBallotFormGUI(this.model,this.election,v,b,this);
    	 }//end method 
    	 
    

    
    
  

    /**
     * This method is usually used for data binding of a "data bean" class
     * to the JavaFX controls. A "bean" class is a simple class with getters
	 * and setters for all properties.
     * Changes to a control are immediately set on the bean and a change to
     * the bean is immediately shown in the control.
     */
    private void initialize() {

    }
}
