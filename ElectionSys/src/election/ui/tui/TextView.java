package election.ui.tui;

import java.time.LocalDate;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import election.business.interfaces.Election;
import election.business.interfaces.ElectionOffice;
import election.business.interfaces.Voter;

public class TextView implements Observer {
	
	public TextView (Observable model) {
		model.addObserver(this);
	}

	@Override
	public void update(Observable o, Object arg) {
		
		if (arg instanceof Voter) {
			Voter voter = (Voter) arg;
			this.displayVoterInfo(voter);
		}
		else if (arg instanceof Election) {
			Election election = (Election) arg;
			this.displayElectionInfo(election);
		}
		else if (arg instanceof List<?>) {
			List<String> winnerList = (List<String>) arg;
			this.displayWinner(winnerList);
		}
	}// End update method
	
	public void displayVoterInfo(Voter voter) {
		System.out.println("Voter information");
		System.out.println("Name: " + voter.getName());
		System.out.println("Email: " + voter.getEmail());
		System.out.println("Postal code: " + voter.getPostalCode());
	}
	
	public void displayElectionInfo(Election election) {
		System.out.println("Election information");
		System.out.println("Name: " + election.getName());
		System.out.println("Start date: " + election.getStartDate());
		System.out.println("End date: " + election.getEndDate());
		
		LocalDate today = LocalDate.now();
		if(election.getEndDate().compareTo(today) >= 0) {
			System.out.println("Election is not yet complete!");
		}
	}
	
	public void displayWinner (List<String> winner) {
		System.out.println("Winner information");
		System.out.println("The winner is " + winner.get(0));
	}
}// End TextView class
