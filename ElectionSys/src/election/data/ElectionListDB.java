package election.data;

import java.io.IOException;
import java.io.Serializable;

import election.business.*;
import election.business.interfaces.Election;
import election.business.interfaces.ElectionFactory;
import election.data.interfaces.*;
import election.data.DuplicateElectionException;
import util.ListUtilities;
import java.util.List;




/**
 * This is the class that manages the Elections database
 * @author Sonya, Shirin
 * @version 09-11-2017

 */
public class ElectionListDB implements Serializable,ElectionDAO {
	
	private static final long  serialVersionUID = 42031768871L;
	
	private List<Election> database;
	private final ListPersistenceObject listPersistenceObject;
	private final ElectionFactory factory;
	
	// Constructors
	/**

	 * This is the one-parameter constructor of ElectionListDB 
	 * @param listPersistenceObject
	 * @throws IllegalArgumentException
	 * @author Sonya
	 */
	public ElectionListDB (ListPersistenceObject listPersistenceObject) {
		
		if (listPersistenceObject == null) {
			throw new IllegalArgumentException ("The listPersistenceOject cannot be null");
		}
		

		this.listPersistenceObject = listPersistenceObject;
		this.database = this.listPersistenceObject.getElectionDatabase();
		this.factory = DawsonElectionFactory.DAWSON_ELECTION;
	}
	
	/**

	 * This is the two-parameter constructor of ElectionListDB 
	 * @param listPersistenceObject
	 * @param factory
	 * @author Sonya
	 */
	public ElectionListDB (ListPersistenceObject listPersistenceObject, ElectionFactory factory) {
		
		if (listPersistenceObject == null) {
			throw new IllegalArgumentException ("The listPersistenceOject cannot be null");
		}
		
		if (factory == null) {
			throw new IllegalArgumentException ("The factory cannot be null");
		}
		

		this.listPersistenceObject = listPersistenceObject;
		this.database = this.listPersistenceObject.getElectionDatabase();
		this.factory = factory;
	}
	

	/** 
	 * This is the overriden toString() method of ElectionListDB
	 * @return String representation
	 * @author Sonya
	 */

	@Override
	public String toString() {
		//int numElections = database.size() - 1; 
		int numElections = database.size() ; 
		String output = "Number of elections in database: " + numElections;
		
		for (int i = 0; i < database.size(); i++) {

			output += "\n" + database.get(i).toString();

		}
		
		return output;
	}
	

	/**
	 * This is a method to add another election to the database
	 * @param election
	 * @throws IllegalArgumentException
	 * @author Sonya
	 */
	@Override
	public void add (Election election) throws DuplicateElectionException {
		
		if (election == null) {
			throw new IllegalArgumentException ("The Election oject cannot be null");
		}
		
		int searchResult = ListUtilities.binarySearch(database, election, 0, database.size() - 1);
		if (searchResult >= 0) {
			throw new DuplicateElectionException("This election already exists in the dataase");
		}
		
		Election electionDeepCopy = factory.getElectionInstance(election);
		int insertionIndex = (searchResult + 1) * -1;
		database.add(insertionIndex, electionDeepCopy);
	} // End add method

	/**
	 * This is a method to make null the database
	 * @param 
	 * @throws  IOException
	 * @author Shirin 
	 */
	
	@Override
	public void disconnect() throws IOException {		
		try {
			this.listPersistenceObject.saveElectionDatabase(this.database);
			this.database = null;
		}

		catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}
	/**
	 * This is a method to find the election with the name we want and return it
	 * @param name string
	 * @throws  InexistentElectionException
	 * @author Shirin 
	 */
	@Override
	public Election getElection(String name)throws InexistentElectionException{
	
	
		Election election1=this.factory.getElectionInstance(name,"single", 2017, 9, 6,2018,8,5,"H3A", "M1Z" ,"2" ,
		"Yes, I want independence","No, I do not want independence");
		
		int indexElection = ListUtilities.binarySearch(database, election1, 0, database.size() - 1);
		if (indexElection < 0) {
			throw new InexistentElectionException("The election with this name does not exist");
		}
		else
		{
			Election election1Copy = factory.getElectionInstance(this.database.get(indexElection));
			return election1Copy;
		}
		
		 
	    }
	 	
	
	
	

}// end ElectionListDB class