package election.data;

import java.io.Serializable;

/**
 * This exception signals that the provided voter doesn't find a match.
 * @author Marian Ruth Lina
 * @version 2017-11-06
 */

public class InexistentVoterException extends Exception implements Serializable {
	private static final long  serialVersionUID = 42031768871L;

	public InexistentVoterException(){
		super("The provided voter doesn't find a match.");
	}

	public InexistentVoterException(String message) {
		super(message);
	}

}
