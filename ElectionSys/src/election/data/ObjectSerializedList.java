package election.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import election.business.interfaces.Election;
import election.business.interfaces.Voter;
import election.data.interfaces.ListPersistenceObject;

import util.Utilities;
/**
 * ObjectSerializedList allow the election system to retrieve/save the database from/to object serialized files.
 * @author shirin
 * @version 2017-11-22
 *
 */ 
public class ObjectSerializedList implements ListPersistenceObject, Serializable{
	private static final long  serialVersionUID = 42031768871L;
	private final String SerVoterFilename;
	private final String SerElectionFilename;
	 /**
	 * Constructor to set 2 string file name which they are serialized file
	 * 
	 * @param String,first last
	 *  @author shirin
     *  @version 2017-11-22
	 */
 	
	public ObjectSerializedList(String SerVoterFilename, String SerElectionFilename) {
		this.SerVoterFilename = SerVoterFilename;
		this.SerElectionFilename = SerElectionFilename;
	}
	
	 /**
		 * override the method inside ListPersistenceObject interface 
		 * Reads in the list of voters from serialized file
		 * 
		 * @param 
		 * @return a list of voters
		 *  @author shirin
	     *  @version 2017-11-22
		 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Voter> getVoterDatabase() {
		 List<Voter> list;
		  
		  try {
			  list = (List<Voter>)Utilities.deserializeObject(this.SerVoterFilename );
			  
		  }catch(Exception e) {
			  return new ArrayList<>();
		  }
		return list;  
		  
	}
	 /**
	 * override the method inside ListPersistenceObject interface 
	 * Reads in the list of elections from serialized file
	 * 
	 * @param 
	 * @return a list of election
	 *  @author shirin
     *  @version 2017-11-22
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Election> getElectionDatabase() {
		List<Election> list;
		  
		  try {
			  list = (List<Election>)Utilities.deserializeObject(this.SerElectionFilename );
			  
		  }catch(Exception e) {
			  return new ArrayList<>();
		  }
		return list; 
	}
	/**
	 * Saves a List of Voters to disk
	 * 
	 * @param voters
	 *            List of Voters to save
	 * @throws IOException
	 * @author shirin
     *  @version 2017-11-22
	 */

	@Override
	public void saveVoterDatabase(List<Voter> voters) throws IOException {
		Utilities.serializeObject(voters, this.SerVoterFilename);
	}
	/**
	 * Saves a List of Elections to disk
	 * 
	 * @param elections
	 *            List of Elections to save
	 * @throws IOException
	 *  @author shirin
     *  @version 2017-11-22
	 */
	@Override
	public void saveElectionDatabase(List<Election> elections) throws IOException {
		Utilities.serializeObject(elections, this.SerElectionFilename);
		
	}

}
