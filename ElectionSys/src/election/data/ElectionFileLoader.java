/**
 * 
 */
package election.data;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DateTimeException;
import java.util.*;

import election.business.DawsonElectionFactory;
import election.business.interfaces.Election;
import election.business.interfaces.Voter;

/**

 * 
 * Read the elections and voters and tally files create array of Election and Voters
 * associate tally to Election
 * 
 * @author Farzaneh, Shirin
 * @version October 23, 2017

 *
 */
public class ElectionFileLoader implements Serializable {
	private static final long  serialVersionUID = 42031768871L;
	
	/**
	 * This prevents instantiation
	 */
	private ElectionFileLoader() {
		
	}
	
	/**
	 * It takes a file's name, find its path then use the file data to create the voter list.
	 * @param filename
	 * @return an array of type Voter
	 * @throws IOException
	 * @author Farzaneh
	 */
	public static Voter[] getVoterListFromSequentialFile (String filename) throws IOException{
		
	    Path filepath = Paths.get(filename);
	    // i keeps the index of voterList that has to be filled
	    int i = 0;
	    //A list of lines in given file
	    List <String> lines = Files.readAllLines(filepath);
	    
	    Voter [] voterList = new Voter[lines.size()];
	    for(String line : lines) {
	    	
	    	//extract the information from each line to create voter instance
	    	String[] segments = line.split("\\*", -1);
	    	
	    	try {
	    		//instantiate voter object and put it in the voter list
	    		voterList[i] = DawsonElectionFactory.DAWSON_ELECTION.getVoterInstance(segments[1], segments[2], segments[0], segments[3]);
	    	
	    		i++;
	    		
	    	}
	    	catch (IllegalArgumentException iae){
	    		System.err.println(iae.getMessage());
	    	}
	   	}// end for
	    
		return Arrays.copyOf(voterList, i);
		
	}//end getVoterListFromSequentialFile()
	/**
	 * It takes a file's name, find its path then use the file data to create the Election list. 
	 * 
	 * @param filename
	 * @return an array of type Election[]
	 * @throws IOException
	 * @author Shirin
	 */
	public static Election[] getElectionListFromSequentialFile(String filename) throws IOException {

		try {
			Path unsorted = Paths.get(filename);
			if (Files.exists(unsorted)) {
            // read the file line by line
				List<String> lines = Files.readAllLines(unsorted);
				if (unsorted == null)
					throw new IllegalArgumentException("input is null and invalid");
                  
				Election[] e1 = new Election[lines.size()];
             
				int i = 0;
				
				for (int j = 0; j < lines.size(); j++)

				{
                  // seprate each line by method split on *
					String[] fields = lines.get(j).split("\\*", -1);
					if (fields.length < 2)
						continue;
                  // create an loop on base of lastIndex  to get how many items each line has then read these lines 
					int lastIndex = fields.length - 1;
					int count = Integer.parseInt(fields[lastIndex]);
					String[] items = new String[count];
					for (int k = 0; k < count; k++) {

						items[k] = lines.get(j + k + 1);
						
					}

					try {
                          // instantiate Election[] by using DawsonElectionFactory
						e1[i] = (DawsonElectionFactory.DAWSON_ELECTION.getElectionInstance(fields[0], fields[9],
								Integer.parseInt(fields[1]), Integer.parseInt(fields[2]), Integer.parseInt(fields[3]),
								Integer.parseInt(fields[4]), Integer.parseInt(fields[5]), Integer.parseInt(fields[6]),
								fields[7], fields[8], items));

					
						i++;

					} catch (IllegalArgumentException iae) {
						System.err.println(iae.getMessage());
					} catch (NullPointerException npe) {
						System.err.println(npe.getMessage());
					}catch (DateTimeException iae) {
						System.err.println(iae.getMessage());
					}

				}

			
				

			 e1=Arrays.copyOf(e1, i);
			
				return e1;
				
			}

			System.out.println("file dose not exist");

		} catch (InvalidPathException ipe) {
			System.out.println("string cannot be converted due to invalid characters");
		} catch (IOException ioe) {
			System.out.println("io error or parent directory doesn�t exist");
		}

		return null;
	}
	/**
	 * It takes a file name and an array of elections, extracts the tally information from the given file, searches through
	 * the array of election for the tally's election then using DawsonElectionFactory associates this election to the tally.
	 * @param filename
	 * @param election
	 * @throws IOException
	 * @author Farzaneh
	 */
	public static void setExistingTallyFromSequentialFile(String filename, Election[] election) throws IOException{
		
		int[][] result;
		Path filePath = Paths.get(filename);
		
		List <String> lines = Files.readAllLines(filePath);
		
		for (int i = 0; i< lines.size(); i++ ) {
			//extract the information from each line 
			String [] segment = lines.get(i).split("\\*", -1);
			
			int choice = Integer.parseInt(segment[1]);
			result = new int[choice][choice];
			
			//creating a 2D array to store the results
			for (int j = 0; j< choice; j++) {
				String[] fields = lines.get(i+j+1).split("\\*", -1);
				for(int k = 0; k < choice; k++ ) {
					try {
						
						result [j][k] = Integer.parseInt(fields[k]);
						
					}
					catch(NumberFormatException nfe) {
						System .err.println(nfe.getMessage()+"\t"+ segment[0] + " choice "+ k+ " is invalid");
					}
				}// end for k
			}//end for j
	
				//looking for extracting election in the given array of elections
				for (int l= 0; l<election.length; l++) {
					try {

						if ( segment[0].equals(election[l].getName()) ) {
							//Associate the result with corresponding election
							DawsonElectionFactory.DAWSON_ELECTION.setExistingTally(result, election[l]);
						}//end if
					}	
						catch (NullPointerException npe) {
							System.err.println(npe.getMessage());
						}
			}// for l
			
			i += choice;
			
		}// end lines
		
	}// end setExistingTally

}

