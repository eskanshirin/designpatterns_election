/**
 * 
 */
package election.data;

import java.io.IOException;
import java.util.List;

import election.business.DawsonElectionFactory;
import election.business.interfaces.*;
import election.data.InexistentVoterException;
import election.data.interfaces.*;
import lib.*;
import util.ListUtilities;

/**
 * @author Lara
 *
 */
public class VoterListDB implements VoterDAO {

	// private fields
	private List<Voter> database; // the file of voters in a form of a list.
	private final ListPersistenceObject listPersistenceObject;// a way to interact with the data base (from file to list
																// or from list to file)
	private final ElectionFactory factory; // the way of creating an object deep copy

	public VoterListDB(ListPersistenceObject listPersistenceObject) {

		// check for null
		if (listPersistenceObject == null) {
			throw new IllegalArgumentException("listPersistenceObject cant be null");
		}
		this.listPersistenceObject = listPersistenceObject;
		this.database = this.listPersistenceObject.getVoterDatabase();
		this.factory = DawsonElectionFactory.DAWSON_ELECTION;
	}

	public VoterListDB(ListPersistenceObject listPersistenceObject, ElectionFactory factory) {
		if (listPersistenceObject == null) {
			throw new IllegalArgumentException("listPersistenceObject cant be null");
		}
		if (factory == null) {
			throw new IllegalArgumentException("factory cant be null");
		}

		this.listPersistenceObject = listPersistenceObject;
		this.database = this.listPersistenceObject.getVoterDatabase();
		this.factory = factory;
	}

	/**
	 *
	 */
	@Override
	public void add(Voter voter) throws DuplicateVoterException {

		if (voter == null) {
			throw new IllegalArgumentException("Voter cant be null");
		}

		// checking if the voter exists
		int index = ListUtilities.binarySearch(this.database, voter, 0, this.database.size()-1);
		// note: database is of type voter, the key has to be of that type to
		if (index >= 0) {
			// if the following line of code runs, the voter was found. Can't add the same
			// one twice.
			throw new DuplicateVoterException("The voter is already in the database,can't add it twice");
		}

		else {
			// if still here, the voter was not found.The position is where it should have
			// been

			/// create a new voter obj using the factory
			this.database.add((index + 1) * -1, this.factory.getVoterInstance(voter));

		}

	}

	@Override
	public void disconnect() throws IOException {
		// this method makes sure the info will be saved in to a file
		try {
			this.listPersistenceObject.saveVoterDatabase(this.database);
			this.database = null;
		}

		catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}

	@Override
	public Voter getVoter(String email) throws InexistentVoterException {
		// return a reference to Voter using the email address.
		// or will throw an exception if voter doesn't exist

		// Create a dummy that will serve as the key in the binary search
		Voter v = this.factory.getVoterInstance("Lara", "Moshkovich", email, "H8T2B1"); // note:email.toString() cant
																						// just have an object there

		int result = ListUtilities.binarySearch(this.database, v, 0, this.database.size()-1);

		if (result < 0) {
			// was not found , throw an exeption
			throw new InexistentVoterException("This voter doesnt exist in the data base");
		}

		else {
			// if it was found
			return this.factory.getVoterInstance(this.database.get(result));
		}
	}

	@Override
	public void update(Email emailWeSearchFor, PostalCode postalCode) throws InexistentVoterException {
		if (emailWeSearchFor == null) {
			throw new IllegalArgumentException("Email cant be null");
		}

		if (postalCode == null) {
			throw new IllegalArgumentException("PostalCode cant be null");
		}
		// if the voter exists you update it
		Voter v = this.factory.getVoterInstance("Lara", "Moshkovich", emailWeSearchFor.toString(), "H8T2B1");

		int result = ListUtilities.binarySearch(this.database, v, 0, this.database.size() - 1);

		if (result < 0) {
			// was not found
			throw new InexistentVoterException("Cant update voter if it doesnt exist in our list");

		}

		else {
			this.database.get(result).setPostalCode(postalCode); 
		}
	}

	@Override
	public String toString() {

		StringBuilder resultToReturn = new StringBuilder("");
		resultToReturn.append("\nThis database has " + this.database.size() + " voters in it\n");
		for (int index = 0; index < this.database.size(); index++) {

			resultToReturn.append("\n"+ this.database.get(index).toString() + "\n");
		}

		return resultToReturn.toString();
	}// end toString()
}
