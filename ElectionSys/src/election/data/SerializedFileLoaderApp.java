package election.data;
import java.io.IOException;
import util.Utilities;

/**
 * This class will load the data from the sequential text files into a Lists. Then serializes them
 * to object serialized files.
 * 
 * @author Marian Ruth Lina
 * @version 2017-11-23
 *
 */


public class SerializedFileLoaderApp {
	
	public static void main (String[] args) throws IOException {
		
		SequentialTextFileList lists = new SequentialTextFileList("datafiles/database/voters.txt",
				"datafiles/database/elections.txt", "datafiles/database/tally.txt");
		
		Utilities.serializeObject(lists.getVoterDatabase(), "datafiles/database/voters.ser");
		Utilities.serializeObject(lists.getElectionDatabase(), "datafiles/database/elections.ser");
	}//end main
}//end SerializedFileLoaderApp
