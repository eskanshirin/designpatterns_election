package election.data;

import java.io.Serializable;

/**
 * This exception signals that the provided election name exists in the database already.
 * @author Marian Ruth Lina
 * @version 2017-11-06
 */

public class DuplicateElectionException  extends Exception implements Serializable{
	private static final long  serialVersionUID = 42031768871L;
	
	public DuplicateElectionException(){
		super("Election's name must be unique. The provided Election name is associated with an existing name.");
	}
	
	public DuplicateElectionException(String message) {
		super(message);
	}

}
