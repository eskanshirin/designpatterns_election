package election.data;

import java.io.IOException;
import election.business.interfaces.Election;
import election.business.interfaces.Voter;
import util.ListUtilities;
import java.nio.file.*;

/**
 * @author shirin ,Farzanie,Sonya,Lara
 *
 */
public class SortMergeApp {

	/**
	 * This applications loads, sorts and merges files from ElectionFileLoader and
	 * ListUtilities
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {

		// load all voters files ,create voter[] for every voters1.txt to voters8.txt

		Voter[][] listOfVoters = new Voter[8][]; 		
		int counterOfVoters = 1; // every file that has been created
		for (int index = 0; index < listOfVoters.length; index++) {
			try 
			{
				listOfVoters[index] = ElectionFileLoader
						.getVoterListFromSequentialFile("datafiles/unsorted/voters" + counterOfVoters + ".txt");
				counterOfVoters++; 
			} catch (IOException ioe) 
			{
				System.err.print(ioe.getMessage());
			} // end message
		} // end for

		// sort all of the voters
		for (int index = 0; index < listOfVoters.length; index++) {
			try 
			{
			ListUtilities.sort(listOfVoters[index]);
			}
			catch(IllegalArgumentException aie) 
			{
				System.err.print(aie.getMessage());           

			}
			
			catch(NullPointerException npe)
			{
				System.err.print(npe.getMessage());

			}
		} // end for

		// put the sorted voter arrays in a file
		Path voterSorted; 
 		voterSorted = Paths.get("/datafiles/sorted");
 		if ( !Files.exists(voterSorted) ) {
 			Files.createDirectory(voterSorted);
 		}
		int counterOfVoters2 = 1;
		for (int index = 0; index < listOfVoters.length; index++) {
			try 
			{
				ListUtilities.saveListToTextFile(listOfVoters[index],
						"datafiles/sorted/voters" + counterOfVoters2 + ".txt");   
				counterOfVoters2++; 

			} catch (IOException ioe) {
				System.err.print(ioe.getMessage());
			}
		} // end for
	
		String file = "/datafiles/database/duplicateVoters.txt";

		Path voterDup;
 		voterDup = Paths.get("/datafiles/database");
 		if ( !Files.exists(voterDup) ) {
 			Files.createDirectory(voterDup);
 		}
		@SuppressWarnings("rawtypes")
		Comparable[] voters = ListUtilities.merge(listOfVoters[0], listOfVoters[1], file);
			for (int index = 2; index < listOfVoters.length; index++) {
				try 
				{
				 voters = ListUtilities.merge(voters, listOfVoters[index], file);
				}
                catch (IllegalArgumentException iae) 
				{
			    System.err.println(iae.getMessage());
		        }
		     index++;  //not to merge the same file again
		
	}//end for 
			
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// load all elections files ,create Election[] for every elections1.txt to ....8
		Election[][] listOfElections = new Election[8][];
		int counterOfElections = 1;
		for (int index = 0; index < listOfElections.length; index++) {
			try 
			{
			listOfElections[index] = 
			ElectionFileLoader.getElectionListFromSequentialFile("datafiles/unsorted/elections" + counterOfElections + ".txt");
			}
			catch(IOException ioe) 
			{
				System.err.print(ioe.getMessage());
			}
			counterOfElections++;
		} // end for

		//sort it
		for (int index = 0; index < listOfElections.length; index++) {
			try 
			{
			ListUtilities.sort(listOfElections[index]);
			}
			catch(IllegalArgumentException aie) 
			{
				System.err.print(aie.getMessage());

			}
			
			catch(NullPointerException npe) 
			{
				System.err.print(npe.getMessage());

			}
		} // end for
		
		
		
		/////should also be in a dir ?? like voterS?
		Path ElecSorted;
 		ElecSorted = Paths.get("/datafiles/sorted");
 		if ( !Files.exists(ElecSorted) ) {
 		Files.createDirectory(ElecSorted);
 		}
       //put the sorted arrays in a file    
		int counterOfElections2 = 1;
		for (int index = 0; index < listOfElections.length; index++) {
			
			try
			{
		     ListUtilities.saveListToTextFile(listOfElections[index],"datafiles/sorted/elections" + counterOfElections2 + ".txt");
		    }
			
			catch(IOException ioe)
			{
			 System.err.println(ioe.getMessage());
			}
		 counterOfElections2++;
		} // end for
		

		String file2 = "datafile/database/duplicateelections.txt";
 
 		Path elecDup;
 		elecDup = Paths.get("/datafiles/database");
 		if ( !Files.exists(elecDup) ) {
 		Files.createDirectory(elecDup);
 		}
		@SuppressWarnings("rawtypes")
		Comparable[] elections2 = ListUtilities.merge(listOfElections[0], listOfElections[1], file2);

		for (int index = 2; index < listOfElections.length; index++) {
			try 
			{
			  elections2 = ListUtilities.merge(elections2,listOfVoters[index], file2);
			}
			catch (IllegalArgumentException iae)
			{
			  System.err.println(iae.getMessage());
		    }
			index++;
			}//end for
		 
		
		try 
		{
		  ElectionFileLoader.setExistingTallyFromSequentialFile("datafiles/unsorted/tally",listOfElections[1]) ;/////ASK SHIRIN /Jaya 
		
		}
		catch(IOException ioe) 
		{
		  System.err.print(ioe.getMessage());
		}
		
		catch(NumberFormatException nfe) {
			  System.err.println(nfe.getMessage());
			
		}		  


		
			

	}// end main
}// end class
