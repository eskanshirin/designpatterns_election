package election.data;

import java.io.Serializable;

/**
 * This exception signals that the provided election doesn't find a match.
 * @author Marian Ruth Lina
 * @version 2017-11-06
 */

public class InexistentElectionException extends Exception implements Serializable {
	private static final long  serialVersionUID = 42031768871L;

	public InexistentElectionException(){
		super("The provided election doesn't find a match.");
	}

	public InexistentElectionException(String message) {
		super(message);
	}

}
