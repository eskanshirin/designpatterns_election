package election.business;
import java.io.Serializable;
import java.util.Comparator; 
import election.business.interfaces.Voter; //ask why need to import this?
public class VoterNameComparator implements Serializable, Comparator<Voter> {
   
	private static final long  serialVersionUID = 42031768871L;
	@Override
	public int compare(Voter a,Voter b) {
		
		return a.getName().compareTo(b.getName());
		//will take the compareTo of Name
	}

}
