
package election.business;

import java.io.Serializable;
import java.time.DateTimeException;
import java.time.LocalDate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import util.ListUtilities;

import election.business.interfaces.Ballot;
import election.business.interfaces.BallotItem;
import election.business.interfaces.Election;
import election.business.interfaces.Tally;
import election.business.interfaces.Voter;

/**
 * @author Shirin Eskandari & Farzaneh Sabzehali
 * @version 28 September 2017
 */
public class DawsonElection implements Serializable, Election  {
	
	private static final long serialVersionUID = 42031768871L;
	private String name;
	private ElectionType type;
	private LocalDate start;
	private LocalDate end;	
	private String startRange;
	private String endRange;
	private Tally tally;
	private BallotItem[] ballotItems;

	private int invalidVoteCtr;
	private List<Voter> got;
	private List<Voter> cast;



	/**
	 * 
	 */
	public DawsonElection(String name, String type, 
			int startYear, int startMonth, int startDay,
			int endYear, int endMonth, int endDay,
			String startRange, String endRange, Tally tally,
			String... items ) {

		//Validate name
		name = name.trim();
		if (name == null || name =="" )
			throw new IllegalArgumentException("The name is null or empty");
		this.name=name;

		//Validate ballot items
		if(items.length<2)
			throw new IllegalArgumentException("ballot items number should be at least 2");
		for(int i=0; i<items.length; i++) {
			items[i] = items[i].trim();
			if(items[i]== null || items[i]=="")
				throw new IllegalArgumentException("One or more items are null or empty");

		}
		ballotItems = new BallotItem[items.length];

		for(int i=0;i<items.length;i++) {
			ballotItems[i] = new DawsonBallotItem(items[i], items[i].length());
		}
		
		for(int i=0;i<items.length;i++) {
			ballotItems[i] = DawsonElectionFactory.DAWSON_ELECTION.getBallotItem(ballotItems[i]);
		}
		
		


		try {

			start = LocalDate.of(startYear, startMonth, startDay);
			end = LocalDate.of(endYear, endMonth, endDay);

		}catch(DateTimeException e){


			throw new IllegalArgumentException("Invalid start date");
		}

		if( end.isBefore(start))
			throw new IllegalArgumentException("Invalid data, The start date cant be after the end date");


		try{
			this.type = ElectionType.valueOf(type.trim().toUpperCase());
		}
		catch(IllegalArgumentException e) {
			throw new IllegalArgumentException("Invalid election type enum"); 
		}


		if(tally == null)
			throw new IllegalArgumentException("Invalid data. object tally can not be null"); 
		this.tally=tally;
		

		
		got = new ArrayList<>();
		
		cast = new ArrayList<>();

		this.startRange = startRange;
		this.endRange = endRange;


	}// end constructor



	@Override
	public int compareTo(Election o) {
		return this.name.compareToIgnoreCase(o.getName());
	}

	@Override

	public ElectionType getElectionType() {	
		return this.type;
	}

	@Override
	public String[] getElectionChoices() {
		String[] choices = new String[this.ballotItems.length];
		for(int i=0;i<this.ballotItems.length;i++) {
			choices[i]=this.ballotItems[i].getChoice();
		} 
		return choices;

	}
	@Override
	public LocalDate getEndDate() {
		
		return this.end;
	}
	@Override
	public LocalDate getStartDate() {
		
		return this.start;
	}
	@Override
	public String getPostalRangeEnd() {
		
		return endRange;
	}
	@Override
	public String getPostalRangeStart() {
		
		return startRange;
	}
	@Override
	public boolean isLimitedToPostalRange() {


		if(startRange!= null || endRange!=null) {
			return true;
		}

		return false;

	}// end isLimitedToPostalRange()
	

	@Override
	public String getName() {
		
		return this.name;
	}

	@Override
	public Tally getTally() {
		return this.tally;
	}// end getTally
	

	
	@Override
	public void setTally(Tally tally) {
		if(tally == null || tally.getElectionName()!=this.name) 
			throw new IllegalArgumentException("Invalid data. object tally can not be null or with different name");
		this.tally = tally;

	}// end setTally
	
	/**
	 * This method takes a voter, validates and search it in list of voters who got a ballot or cast a ballot.
	 * It will throw exception or add the voter in the list of voters who got a ballot
	 * @author Farzaneh November 10, 2017
	 * @param v a voter
	 * @throws InvalidVoterException
	 * @return Ballot
	 */
	@Override
	public Ballot getBallot(Voter v) {

		if (v.isEligible(this)) {
			int gotIndex = ListUtilities.binarySearch(got, v, 0, got.size()-1);
			
			if (gotIndex < 0) {
				got.add(-gotIndex - 1, v);
			} // if gotIndex
			else {
				int castIndex = ListUtilities.binarySearch(cast, v, 0, cast.size()-1);
				
				if (castIndex >= 0)
					throw new InvalidVoterException("The voter " + v.getName() + " has already cast a ballot!");
			} // else

		} // if isEligible
		else
			throw new InvalidVoterException("The voter " + v.getName() + " is not eligible for " + this.name);

		BallotItem[] copy = new BallotItem[ballotItems.length];
		for (int i = 0; i < ballotItems.length; i++) {
			copy[i] = DawsonElectionFactory.DAWSON_ELECTION.getBallotItem(ballotItems[i]);
		}
		StubBallot stubBallot = new StubBallot(copy, this);
		return stubBallot;
	}// end getBallot

	
	/**
	 * This method takes a ballot and a voter, validates voter and searches it in a list of voters who got a ballot and who cast
	 * a ballot. Depending on the result, it will throw an exception or add the voter to the list of voters who cast a vote.
	 * It also calculate invalid vote attempts the number of  
	 * @author Farzaneh November 10, 2017
	 * @param Ballot
	 * @param Voter
	 * @throws InvalidVoterException
	 */
	@Override
	public void castBallot(Ballot b, Voter v) {

		if (v.isEligible(this)) {
			int gotIndex = ListUtilities.binarySearch(got, v, 0, got.size()-1);

			if (gotIndex < 0)
				throw new InvalidVoterException("The voter " + v.getName() + " has never requested a ballot.");

			int castIndex = ListUtilities.binarySearch(cast, v, 0, cast.size()-1);
			if (castIndex >= 0) {
				invalidVoteCtr++;
				throw new InvalidVoterException("The voter " + v.getName() + " has already cast a ballot.");

			} // if castIndex
			else
				cast.add(-castIndex - 1, v);
		} // if isEligible
		else {
			invalidVoteCtr++;
			throw new InvalidVoterException("The voter " + v.getName() + " is not eligible for " + this.name);

		} // else isEligible

			if (b.validateSelections())
				this.tally.update(b);
			else
				throw new IllegalArgumentException("The ballot has not been filled correctly.");

	}// end castBallot
	
	
	
	@Override
	public int getTotalVotesCast() {
		return cast.size();

	}
	@Override
	public int getInvalidVoteAttempts() {
		return invalidVoteCtr;
	}

	@Override
	public final boolean equals(Object obj){
		if (this == obj)
			return true;
		if ( obj == null)
			return false;

		if (!(obj instanceof DawsonElection))
			return false;
		DawsonElection other = (DawsonElection)obj;

		return this.name.equalsIgnoreCase(other.getName()) ;

	}// end equals()

	
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;

	}
	
	@Override
	public String toString()
	{

		String result = "";
		result = this.name+"*"+this.start.getYear()+"*"+this.start.getMonthValue()+"*"+this.start.getDayOfMonth()+"*"+
				+this.end.getYear()+"*"+this.end.getMonthValue()+"*"+this.end.getDayOfMonth()+"*";
		if(this.startRange !=null)
			result+= this.startRange+"*";
		if(this.endRange!=null)
			result+= this.endRange+"*";


		result+= this.type.toString()+"*"+ this.ballotItems.length+"*"+" Options";

		for(int i=0;i<this.ballotItems.length;i++) {
			result += "\n"+this.ballotItems[i].getChoice();
		}
		return result;

	}// end toString()
}// end class



