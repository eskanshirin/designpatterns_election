package election.business;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import election.business.interfaces.Election;
import election.business.interfaces.ElectionPolicy;
import election.business.interfaces.ElectionFactory;
import election.business.interfaces.Tally;

public class DawsonSingleElectionPolicy implements ElectionPolicy {
	
	private static final long serialVersionUID = 42031768871L;
	private Election election;
	
	public DawsonSingleElectionPolicy (Election election) {
		if (election == null) {
			throw new IllegalArgumentException("The election object cannot be null");
		}
		
		if ( !(election.getElectionType().toString().equals("single")) ) {
			throw new IllegalArgumentException("The election type that you provided is not single");
		}
		
		this.election = election;
	}

	@Override
	public List<String> getWinner() {
		List<String> winner = new ArrayList<>();
		
		LocalDate now = LocalDate.now();
		
		LocalDate endDate = election.getEndDate();
		
		if (endDate.compareTo(now) > 0) {
			throw new IncompleteElectionException("The election is not over yet");
		}
		Tally tally = this.election.getTally();
		int[][] numVotesCandidate = tally.getVoteBreakdown();
		
		// List of candidates
		List <String>electionChoices = new ArrayList<String>(Arrays.asList( election.getElectionChoices()));
		
		// Total number of votes
		int totalNumVotes = 0;
		for (int i = 0; i < numVotesCandidate.length; i++) {
			totalNumVotes += numVotesCandidate[i][i];
		}
		
		int numVotes;
		for (int i = 0; i < electionChoices.size(); i++) {
			numVotes = numVotesCandidate[i][i];
			
			if (numVotes > (totalNumVotes / 2)) {
				winner.add(electionChoices.get(i));
			}
		}
		
		return winner;
	}// End getWinner method
}// End DawsonSingleElectionPolicy class
