/**
 * @author Lara
 */
package election.business;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import election.business.interfaces.Election;
import election.business.interfaces.ElectionPolicy;


public class DawsonRankedElectionPolicy implements ElectionPolicy,Serializable{

	private Election election;
	private static final long serialVersionUID = 42031768871L;

	// con
	public DawsonRankedElectionPolicy(Election electionFromUser) {
		if ((electionFromUser.getElectionType() == ElectionType.SINGLE)) 
		{
			 throw new IllegalArgumentException("The election type that you provided is not ranked");
		}
		this.election = electionFromUser;
	}


	@Override
	public List<String> getWinner() {
		// if election is not complete yet , will not do any of that
		if (election.getEndDate().isAfter(LocalDate.now())) {
			throw new IncompleteElectionException("The election is not over yet");
		}

		String[] electionChoices = election.getElectionChoices();// same length
		int[][] list = election.getTally().getVoteBreakdown();
		int[] points = new int[list.length];// same length
		int result = 0;
		List<String> retArray = new ArrayList<String>();
		for (int index = 0; index < list.length; index++) {
			for (int index2 = 0; index2 < 2; index2++) {
				if (index2 == 0) {
					result = list[index][index2] * 5;
				}
				//if (index2 == 1) {
				else {	
				result = result + list[index][index2] * 2;
				} // end if

				points[index] = result;

			} // end mini for loop

		} // end big foor loop
			// find the max
		int max = points[0];
		for (int indexOfMax = 1; indexOfMax < points.length; indexOfMax++) {
			if (max < points[indexOfMax]) {
				max = points[indexOfMax];
			}

			// find the winners
			for (int index = 0; index < points.length; index++) {
				if (points[index] == max)
					retArray.add(electionChoices[index]);//Parallel arrays
			}

		}
		return retArray;
	}// end method

}// end class
