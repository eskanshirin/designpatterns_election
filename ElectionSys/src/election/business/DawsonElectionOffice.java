/**
 * 
 */
package election.business;

import java.io.IOException;
import java.util.List;
import java.util.Observable;

import election.business.interfaces.Ballot;
import election.business.interfaces.Election;
import election.business.interfaces.ElectionOffice;
import election.business.interfaces.Voter;
import election.business.interfaces.ElectionFactory;
import election.data.DuplicateElectionException;
import election.data.DuplicateVoterException;
import election.data.InexistentElectionException;
import election.data.InexistentVoterException;
import election.data.interfaces.ElectionDAO;
import election.data.interfaces.VoterDAO;

/**
 * @author Farzaneh
 * @version November 16, 2017
 *
 */
public class DawsonElectionOffice extends Observable implements ElectionOffice {
	
	private final ElectionFactory factory;
	private final ElectionDAO elections;
	private final VoterDAO voters;
	private static final long serialVersionUID = 420317688871L;
	
	public DawsonElectionOffice(ElectionFactory factory, ElectionDAO elections, VoterDAO voters) {
		this.factory = factory;
		this.elections = elections;
		this.voters = voters;
	}

	
	@Override
	public Ballot getBallot(Voter voter, Election election) throws InvalidVoterException {
		
		return election.getBallot(voter);

	}

	
	@Override
	public void castBallot(Voter voter, Ballot b) {
		
		b.cast(voter);

	}

	@Override
	public void closeOffice() throws IOException {
		this.elections.disconnect();
		this.voters.disconnect();

	}

	/* 
	 * 
	 */
	@Override
	public Election createElection(String name, String type, int startYear, int startMonth, int startDay, int endYear,
			int endMonth, int endDay, String startRange, String endRange, String... choices)
			throws DuplicateElectionException {
		try {
			this.elections.getElection(name);
			throw new DuplicateElectionException("This election already exists.");
		}
		catch (InexistentElectionException iee) {
			System.out.println(iee.getMessage());
		}
		
		Election e = this.factory.getElectionInstance(name, type, startYear, startMonth, 
				startDay, endYear, endMonth, endDay, startRange, endRange, choices);
		elections.add(e);
		return e;
		
		
	}

	
	@Override
	public List<String> getWinner(Election election) {
		
		List<String> winner = this.factory.getElectionPolicy(election).getWinner();
		setChanged();
		notifyObservers(winner);
		return winner;
	}

	
	@Override
	public Voter registerVoter(String firstName, String lastName, String email, String postalcode)
			throws DuplicateVoterException {
		try {
			this.voters.getVoter(email);
			throw new DuplicateVoterException("This voter has already registered.");
		}
		catch(InexistentVoterException ive){
			System.out.println(ive.getMessage());
		}
		
		Voter voter = this.factory.getVoterInstance(firstName, lastName, email, postalcode);
		voters.add(voter);
		setChanged();
		notifyObservers(voter);
		return voter;
	}

	
	@Override
	public Election findElection(String name) throws InexistentElectionException {
		
		Election election = this.elections.getElection(name);
		setChanged();
		notifyObservers(election);
		return election;
	}

	
	@Override
	public Voter findVoter(String email) throws InexistentVoterException {
		
		Voter voter  = this.voters.getVoter(email);
		setChanged();
		notifyObservers(voter);
		return voter;
	}
	
	@Override
	public Voter findVoter (String email, boolean notify) throws InexistentVoterException{
		
		Voter voter = this.voters.getVoter(email);
		if (notify) {
			setChanged();
			notifyObservers(voter);
		}
		return voter;
	}

}
