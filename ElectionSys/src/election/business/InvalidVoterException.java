package election.business;

import java.io.Serializable;

/**
 * This exception will be thrown if a voter tries to get or cast a ballot for an election which is either
 * not in progress, or for which the voter is not eligible, or for which the voter has already voted.
 * 
 * @author Marian Ruth Lina
 * @version 2017-11-07
 */

public class InvalidVoterException extends RuntimeException implements Serializable {
	private static final long  serialVersionUID = 42031768871L;
	
	public InvalidVoterException(){
		super("The ballot that you are trying to cast or get is either not in progress, or either the voter is not eligible"
				+ "or the voter has already voted.");
	}

	public InvalidVoterException(String message) {
		super(message);
	}

}

