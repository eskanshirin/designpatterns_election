/**
 * Dawson Voter class, a logical class that represents a voter and determines if he is eligible
 *@author Lara 
* @version 2017-09-18
 */
package election.business;
import  java.time.LocalDate;



import election.business.interfaces.Election;
import election.business.interfaces.Voter;

import lib.Email;
import lib.Name;
import lib.PostalCode;

import java.io.Serializable;
import java.lang.Comparable;
import java.time.LocalDate;

public class DawsonVoter implements Serializable, Voter, Comparable <Voter>
{
	    private static final long serialVersionUID = 42031768871L;
		private Name name;
		private Email email;
		private PostalCode postalCode;
	
		
	
		/**
		 * Constructor
		 * 
		 * @param String
		 * firstName, String lastName, String email, String postalCode
		 */
	 public DawsonVoter(String firstName, String lastName, String email, String postalCode) {
			 
			this.name = new Name(firstName, lastName);
			this.email = new Email(email);
			this.postalCode = new PostalCode(postalCode);
			
		}
		 
		 /**
		 * @return this.name instance
		 */
	 public Name getName() {
			return this.name;
	 }
	
		/**
		 * @return this.email instance
		 */
	 public Email getEmail() {
			return this.email;
	 }
	
		/**
		 * @return this.postalCode instance
		 */
	public PostalCode getPostalCode() {
			return this.postalCode;
	}
	
		/**
		 * @param postalCode
		 *the postalCode to set
		 */
	public void setPostalCode(PostalCode postalCode) {
			
		this.postalCode = new PostalCode( postalCode.getCode() ); 
		
	}
	
		/**
		 * this method will check the current time within the start/end date range of
		 * the Election
		 * 
		 * @return true if the current time is within the range, otherwise return false
		 * @param objIn
		 */
	   public boolean isEligible(Election object) {
	    	
		   boolean result=false;
	    
		   LocalDate now = LocalDate.now();
		
		   if(now.isAfter(object.getStartDate()) && now.isBefore(object.getEndDate()))
		   {
			   result=true;
			   
				 if(object.isLimitedToPostalRange()) 
				 {
				     if( !(this.postalCode.inRange( object.getPostalRangeStart(), object.getPostalRangeEnd() ) ) )
				     { 
				        result = false;
				     }
				 }//end if postalCodeRange
				 
		
			}//end if limited
		   
		    return result; 
	   }// end isEligible
	    
	
		/**
		 * @param obj
		 * @return true if 2 instances are the same and their email attributes are equal
		 */
		@Override
	public final boolean equals(Object obj) {
			
			// have the same ref.
			if (this == obj) {
				return true;
			}
			// check if obj is null
			if (obj == null) {
				return false;
			}
	
			// check if obj. is another instance of Voter
			if (!(obj instanceof Voter)) {
				return false; // not a Voter or a subclass of Voter.
			}
	
			// if still hasn't returned, we cast
			Voter other = (Voter) obj;
			
			return (this.getEmail().equals(other.getEmail()));
      
	}// end equals()
	
		/**
		 * return int that represents a memory address
		 */
		@Override
	public final int hashCode() {
	
			return email.hashCode();
			
	}// end hashCode()
		
	
		/**
		 * @return a String that has all of the user info:email,first name, last name
		 *         and postal code (ordered respectively)
		 */
		@Override
	public String toString() {
			
			return email.toString() + "*" + name.toString()+ "*" + postalCode.toString();
			
	}// end toString()
		
	
		/**
		 * @param Voter obj
		 * @return an int 0 if the same, -tive if first compared  is smaller then second compared ,+itive if first compared
		 * is bigger then second compared.
		 */
	public int compareTo( Voter obj ) {
			
			return this.email.compareTo(obj.getEmail());

	}//end compareTo()
	
}// end DawsonVoter
