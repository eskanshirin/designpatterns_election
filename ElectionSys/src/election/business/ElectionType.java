package election.business;

import java.io.Serializable;

/**
 * Enum for the different types of elections supported by
 * the system. A SINGLE election implies that only one choice
 * can be selected on the ballot, whereas RANKED means all 
 * choices are given a ranking
 * @author Jaya, Maja
 *
 */
public enum ElectionType implements Serializable {
	SINGLE, RANKED;
	private static final long serialVersionUID = 42031768871L;
	@Override
	public String toString() {
		return this.name().toLowerCase();
	}

}
