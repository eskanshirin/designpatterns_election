
/**
 * This class represents a DawsonBallotItem (the choices the voter has)

 * @author Sonya (JavaDocs,Lara)
 * @version 24-09-2017
 */
package election.business;

import java.io.Serializable;
import election.business.interfaces.BallotItem;
import java.lang.Comparable;

public class DawsonBallotItem implements Serializable, BallotItem
{
	private static final long serialVersionUID = 42031768871L;
	private String choice = " ";
	private int value;
	private final int MAX_VALUE;
	


	/**
	 * 2 param constructor for initialization
	 * 
	 * @param String
	 *newChoice,int newMaxVelue)
	 **/

	// 2 parameters constructor
	public DawsonBallotItem(String newChoice, int maxValue) {

		if (newChoice == null) {
			throw new IllegalArgumentException("The string Choice can not be null");
		}

		if (newChoice.trim().length() == 0) {
			throw new IllegalArgumentException("Choice can not be empty");
		}


		if (maxValue < 1) {


			throw new IllegalArgumentException("The maxValue can not be lower than 1");
		}

		this.choice = newChoice;
		this.MAX_VALUE = maxValue;
		this.value = 0;
	}
 
	/**
	 * a copy constructor to initialize values from another obj to this. instance
	 * @param newBallotItem
	 */

	public DawsonBallotItem(BallotItem newBallotItem) {
		this.choice = newBallotItem.getChoice();
		this.MAX_VALUE = newBallotItem.getMaxValue();
		this.value = newBallotItem.getValue();
	}

	/**
	 * @param newValue
	 */
	public void setValue(int newValue) {
		if (newValue < 0 || newValue > MAX_VALUE) {
			throw new IllegalArgumentException("The value cannot be lower than 0 and cannot be greater than MAX_VALUE");
		}
		this.value = newValue;
	}

	/**
	 * overiding the equals method , two objects are equal if their choice is the
	 * same
	 * 
	 * @param Object
	 *            obj
	 * @return boolean if equal(true) , if not (false)
	 */

	@Override
	public boolean equals(Object object) {
		// Compare choice
		if (this == object) {
			return true;
		}

		if (object == null) {
			return false;
		}

		if (!(object instanceof DawsonBallotItem)) {
			return false;
		}

		DawsonBallotItem newBallot = (DawsonBallotItem) object;

		return (this.choice.equals(newBallot.choice));

	}

	/**
	 * @return int
	 */
	@Override
	public int hashCode() {
		// HashCode of Choice
		return this.choice.hashCode();
	}

	/**
	 * @return String
	 */
	@Override
	public String toString() {
		return (this.choice + "*" + this.value);
	}

	/**
	 * @param Voter
	 *            obj
	 * @return an int 0 if the same, -tive if first compared is smaller then second
	 *         compared ,+itive if first compared is bigger then second compared.
	 */

	@Override
	public int compareTo(BallotItem newBallotItem) 
	{
		return (this.getChoice().compareToIgnoreCase(newBallotItem.getChoice()) );
	}

	@Override
	public String getChoice() 
	{
		return this.choice;
	}

	@Override
	public int getMaxValue() 
	{
		
		return this.MAX_VALUE;

	}

	@Override
	public int getValue() {

		return this.value;
	}


}
