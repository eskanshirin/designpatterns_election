package lib;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This is Email logical class 
 * @author Sonya
 * @version 26-09-2017
 */
public class Email implements Serializable, Comparable <Email>
{
	private static final long serialVersionUID = 42031768871L;
    private String address;
    
    /**
     * Constructor of the Email class
     * @param address
     * @author Sonya
     * @version 22-10-2017
     */
    public Email(String address)
    {
        if(! (validateEmail(address.trim())) )
        {
            throw new IllegalArgumentException("The address is invalid");
        }
        
        this.address = address.trim();
    }
    
    /**
     * Getter for String address
     * @return address
     * @author Sonya
     */
    public String getAddress()
    {
        return this.address;
    }
    
    /**
     * Getter for the userID of address 
     * @return userID
     * @author Sonya 
     */
    public String getUserID()
    {
        int atSignIndex = this.address.indexOf('@');
        
        String userID = this.address.substring(0, atSignIndex);
        
        return userID;
    }
    
    /**
     * Getter for the host of address
     * @return host
     * @author Sonya
     */
    public String getHost()
    {
        int atSignIndex = this.address.indexOf('@');
        
        String host = this.address.substring(atSignIndex + 1);
        
        return host;
    }
    
    /** 
     * Overriden .equals method
     * @param object
     * @return boolean
     * @author Sonya
     */
    @Override
    public boolean equals(Object object)
    {
        if(this == object)
        {
            return true;
        }
        
        if(object == null)
        {
            return false;
        }
        
        if( !(object instanceof Email) )
        {
            return false;
        }
        
        Email newEmail = (Email) object;
        
        return (this.address.equals(newEmail.address));
    }
    
    /**
     * Overriden .hashCode method
     * @return hashCode
     * @author Sonya
     */
    @Override
    public int hashCode()
    {
        return this.address.hashCode();
    }
    
    /** 
     * Overriden .toString method
     * @return address
     * @author Sonya
     */
    @Override
    public String toString()
    {
        return this.address;
    }
    
    /**
     * Overriden .compareTo email
     * @param email
     * @return
     */
    public int compareTo(Email email)
    {
    	return this.address.compareTo(email.address);
    }
    
    /**
     * Validates the email address in the argument
     * @param email
     * @return boolean
     * @author Sonya
     */
    private boolean validateEmail (String email)
    {
    	email = email.trim();
    	
    	// Check if the argument is an empty string
    	if (email.length() == 0) {
    		return false;
    	}
        int atSignIndex = email.indexOf('@');
        
        // Validate place of @ sign
        if ( (atSignIndex == -1) || (atSignIndex == 0) || (atSignIndex == email.length() - 1) )
        {
        	return false;
        }
        
        String userID = email.substring(0, atSignIndex);
        String host = email.substring(atSignIndex + 1);
        
        // Validate the userID has the right characters
        for (int i = 0; i < userID.length(); i++) {
        	if ( !(userID.charAt(i) >= '0' && userID.charAt(i) <= 'z') && (userID.charAt(i) != '.') && (userID.charAt(i) != '-')
        			&& (userID.charAt(i) != '_') ) {
        		return false;
        	}
        }
        
        // Validate the host has the right characters
        for (int i = 0; i < host.length(); i++) {
        	if ( !(host.charAt(i) >= '0' && host.charAt(i) <= 'z') && (host.charAt(i) != '.') && (host.charAt(i) != '-') ) {
        		return false;
        	}
        }
        
        // Validate there is no dot as a first or last character of the userID        
        if ( (userID.charAt(0) == '.') || (userID.charAt(userID.length() - 1) == '.') )
        {
        	return false;
        }
        
        // Validate that there is no two consecutive dots
        int indexDot = email.indexOf('.');
        while (indexDot != -1)
        {
        	if(email.charAt(indexDot + 1) == '.')
        	{
        		return false;
        	}
        	
        	indexDot = email.indexOf('.', indexDot + 1);        	
        }
     
        // Validate length of userID
        if ( (userID.length() > 32) || (userID.length() < 1) )
        {
        	return false;
        }
        
        // Validate hosts by creating an array containing each host (if there are multiple)
        int indexDotHost = 0;
        int numHosts = 0;
        
        while (indexDotHost != -1)
        {
        	numHosts++;
        	indexDotHost = host.indexOf('.', indexDotHost + 1);	
        }
        
        String[] hosts = new String[numHosts];
        
        int indexBegin = 0;
        int i = 0;
        int indexEnd = host.indexOf('.');
        
        while (indexEnd != -1)
        {
        	hosts[i] = host.substring(indexBegin, indexEnd);
        	indexBegin = indexEnd + 1 ;
        	indexEnd = host.indexOf('.', indexBegin + 1);
        	i++;
        }
        
        hosts[numHosts - 1] = host.substring(host.lastIndexOf('.') + 1, host.length());
        
        // Validate lengths of each host and validate if it starts or ends with a -
        for (int j = 0; j < hosts.length; j++)
        {
        	if ( (hosts[j].length() > 32) || (hosts[j].length() < 1) || (hosts[j].charAt(0) == '-') || (hosts[j].charAt(hosts[j].length() - 1) == '-'))
        	{
        		return false;
        	}
        }
        
        return true;
    } // End validate method
} // End Email class
