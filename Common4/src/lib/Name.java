
/**
 * 
 * 
 */ 
package lib;
import java.io.Serializable;
/**
 * The Name class which get firstname and last name and validate them.
 * @author shirin
 * @version 2017-09-28
 *
 */ 


	public class Name implements Serializable,  Comparable<Name>{
		
		private static final long serialVersionUID = 42031768871L;
		 private final String firstName ;
		 private final String lastName ;
		 
		 /**
			 * Constructor to set first and last name and validate them in validate method
			 * 
			 * @param String,first last
			 *  @author shirin
             *  @version 2017-09-28
			 */
		 
		 public Name(String first, String last)
		 {
		
		   this.firstName= validate(first);
		
		   this.lastName=validate(last);
		 }//end constructor
		 
		   /**
		     *  getter for firstName 
			 * 
			 *  @param 
			 *  @return firstName
			 *  @author shirin
             *  @version 2017-09-28
			 */
		 
		 
		 public String getFirstName()
		 {
		   return firstName;
		 }//end getFirstName method
		  
		   /**
		     *  getter for lastName 
			 * 
			 *  @param 
			 *  @return lastName
			 *  @author shirin
             *  @version 2017-09-28
			 */
		
		 public String getLastName()
		 {
		   return lastName;
		 }//end getLastName method
		 
		   /**
		     *  getter for fullName
			 * 
			 *  @param 
			 *  @return fullName
			 *  @author shirin
             *  @version 2017-09-28
			 */
		 
		 public String getFullName()
		 {
		   return this.firstName +" " + this.lastName;
		 }//end getFullName method
		 
		   /**
		     *   Overriden equals method
			 * 
			 *  @param an object of type Object
			 *  @return true if 2 objects are equal(first and last name)(case insensitive)
			 *  @author shirin
             *  @version 2017-09-28
			 */
		
		@Override
		 public final boolean equals(Object obj){
		        if (this == obj)
		         return true;
		        if ( obj == null)
		         return false;
		        
		         if (!(obj instanceof Name))
		         return false;
		       Name other = (Name)obj;
		      
		        return this.getFirstName().equalsIgnoreCase(other.getFirstName()) &&
		          this.getLastName().equalsIgnoreCase(other.getLastName())&&
		          this.getFullName().equalsIgnoreCase(other.getFullName());
		      
		 }//end equals method
		
		 /**
	     *   Overriden hashcode  method
		 * 
		 *  @param
		 *  @return int hashcode
		 *  @author shirin
         *  @version 2017-09-28
		 */
		 
		 @Override
		 public final int hashCode()
		 {
		   int hashCode=this.firstName.toUpperCase().hashCode();
		   hashCode += this.lastName.toUpperCase().hashCode();
		
		   
		   return hashCode;
		 }// end hashcode method
		 
		 /**
		     *   Overriden toString  method
			 * 
			 *  @param
			 *  @return String
			 *  @author shirin
	         *  @version 2017-09-28
			 */
		 
		 @Override
		 
		   public String toString()
		 {
		   return this.firstName+"*"+this.lastName;
		 }//end toString method
		 
		   /**
		     *   compareTo  method
			 * 
			 *  @param object of name
			 *  @return int ,properly
			 *  @author shirin
	         *  @version 2017-09-28
			 */
		 
		 
		 public int compareTo(Name name)
		 {
		   if(this.lastName.compareToIgnoreCase(name.lastName)==0)
		     return this.firstName.compareToIgnoreCase(name.firstName);
		   
		   return this.lastName.compareToIgnoreCase(name.lastName);
		 }//end of compareTo method
		 
		   /**
		     *   validate  method checks first name and lastname from constructor and
		     *   throws exception if it is not appropriate
			 * 
			 *  @param String input
			 *  @return String
			 *  @author shirin
			 *  @throws IllegalArgumentException
	         *  @version 2017-09-28
			 */
		 
		 private String validate(String input)
		  {
		   char ch=' ';
		   int count=0;
		   if (input == null) 
		    throw new IllegalArgumentException("input is null and invalid");

		   if (input=="")
		    throw new IllegalArgumentException("input is empty and invalid");
		   input= input.trim();
		   for(int i=0; i<input.length(); i++){
		         ch = input.charAt(i);
		        if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
		          count++;}

		        if(count <2)
		      
		          throw new IllegalArgumentException("invalid input, input can not be less than two letters");
		     
		   

		         if(!input.matches("[a-zA-z'\\s-]*"))

		   throw new IllegalArgumentException("invalid input,special characters can be only space, apostrophe and hyphen "); 
		else
		  return input;
		   }//end of validate method
		 }//end class


		   
		 
		 
		 
		 
		 



