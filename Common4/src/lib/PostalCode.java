
/**
 * 
 */
package lib;
import java.io.Serializable;
/**
 * @author Farzaneh Sabzehali
 * @version 25 September 2017
 *
 */
public class PostalCode implements Serializable, Comparable <PostalCode>{
	private static final long serialVersionUID = 42031768871L;
	private final String code;
	
	/**
	 * Checks the given code by validate method
	 * @param code
	 * @throws IllegalArgumentException if given code does not satisfy required factors
	 */
	public PostalCode (String code) throws IllegalArgumentException {

	
			this.code = validate (code);

	}//end constructor
	
	/**
	 * Use String class compareTo()
	 * @param an object of type PostalCode
	 * @return  an integer 
	 */
	
	@Override
	public int compareTo(PostalCode code) {
		return this.code.compareToIgnoreCase(code.code);
	} // end compareTo()
	
	/**
	 * Returns true if the codes of 2 PostalCode are case insensitively equal.
	 * @param Takes an object
	 * @return  
	 */
	@Override
	public final boolean equals(Object object) {
		if (this == object )
			return true;
		if (object == null)
			return false;
		if (! (object instanceof PostalCode))
			return false;
		PostalCode newCode = (PostalCode) object;
		if (this.code.equalsIgnoreCase(newCode.code))
			return true;
		return false;
			
	}// end equals()
	
	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code.hashCode();
		return result;
	} //end hashCode()

	/**
	 * @return the code .
	 */
	public String getCode() {
		

		return this.code;

	}//end getCode()
	
	/**
	 * Verify if the PostalCode is in the given range.  
	 * @param start: a String that defines the beginning of the range inclusively
	 * @param end: a String that end the beginning of the range inclusively
	 * @return true if given code is in the defined range
	 */
	public boolean inRange(String start, String end) {
		
		// Check that the both of start and end don't be null
		if(start == null && end == null)
			return false;
		
		// Assume that a not provided start or end(they can't be null at the same time) 
		// means there is no limit so replace the null with empty String to avoid probably code crashing 
		if (start == null)
			start = "";
		if (end == null)
			end = "";
		
		start = start.replaceAll("\\s", "");
		end = end.replaceAll("\\s", "");
		String pat = "^[a-zA-Z0-9]*$";
		if (!((start+end).matches(pat)))
			throw new IllegalArgumentException ("Sart and end range must be alphanumeric."); 
	
		start = start.toUpperCase();
		end = end.toUpperCase();
		
		for (int i = 0; i< start.length(); i++) {
			
			//Iterate through the code and start, if the corresponding character in given code is before start's character so the code is not in the range then return false
			if (this.code.charAt(i) < start.charAt(i))
				return false;
			//If the corresponding character in given code is after start's character break this iteration and go to check with end
			else if(this.code.charAt(i) > start.charAt(i))
				break;
		}// end for (int i = 0; i< start.length(); i++)
		
		for (int j=0; j< end.length(); j++) {
			
			//Iterate through the code and end if the corresponding character in given code is after end's character so the code is not in the range then return false
			if (this.code.charAt(j) > end.charAt(j))
				return false;
			//If the corresponding character in given code is before end's character break this iteration so the code is in the range then stop iteration
			else if (this.code.charAt(j) < end.charAt(j))
				break;
		}// end for (int j=0; j< end.length(); j++)
		return true;
	}// end inRAnge()
	
	/**
	 * @return the code
	 */
	@Override
	public String toString() {
		return this.code;
	}// end toString
	
	/**
	 * Checks the given code to don't be null and has the proper format of postal code ANA NAN or ANANAN
	 * @param code
	 * @return a string that is upper case and without space in the middle
	 * @throws IllegalArgumentException
	 */
	private static String validate(String code) throws IllegalArgumentException {
		
		//Create an array of not permitted characters
		char[] notPermittedChar = {'D','F','I','O','Q','U'};
		
		if (code == null)
			throw new IllegalArgumentException("Postal code can't be null. Please provide a postal code.");
		
		code = code.trim().toUpperCase();
		// Remove the space between the two parts of postal code
		if (code.length() == 7)
			code = code.substring(0, 3) + code.substring(4); 
		
		if (code.length() != 6)
			throw new IllegalArgumentException("Postal code has to be 6 or 7 characters. Please provide a valid postal code.");
	
		for (int i=0; i< code.length(); i++) {
			//Find the alphabetic positions
			if (i%2 == 0) {
				if (!Character.isLetter(code.charAt(i))) 
					throw new IllegalArgumentException("Postal code even characters have to be letter.");
				
				//Check the first alphabet not to be W or Z
				if(i == 0 && (code.charAt(i) == 'W' || code.charAt(i)== 'Z') )
					throw new IllegalArgumentException("The firs letter can't be 'w' or 'z'.");
				
				for (char c:notPermittedChar) {
					if (code.charAt(i) == c)
						throw new IllegalArgumentException("Postal code can't contain d, f, i, o, q and u.");
				}// end inner for
			}//end if (i%2 == 0)
		else {
				if (! Character.isDigit(code.charAt(i)))
					throw new IllegalArgumentException("Postal code odd charachters have to be number.");
			}//end else		
		} //end outer for
		return code;
	}// end validate() 


}
