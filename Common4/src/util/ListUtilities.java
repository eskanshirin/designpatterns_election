package util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Comparator;
import java.lang.reflect.Array;
import java.util.*;
import java.lang.Comparable;
//skeleton provides the imports, plus methods saveListToTextFile and the Comparator sort overload

public class ListUtilities {
	private static final Charset CHARACTER_ENCODING = StandardCharsets.UTF_8;

	/**
	 * private no @param constructor to avoid instantiation
	 */
	private ListUtilities() {
	}

	/**
	 * Takes in a list of objects and writes them to a given file. This method
	 * overwrites data in file and uses UTF8 character set.
	 * 
	 * @param objects
	 *            Array of items to be written to file.
	 * @param filename
	 *            filename and location of the file where list of items will be
	 *            written.
	 * @throws IOException
	 *             if an I/O error occurs writing to or creating the file
	 */
	public static void saveListToTextFile(Object[] objects, String filename) throws IOException {
		saveListToTextFile(objects, filename, false, CHARACTER_ENCODING);
	}

	/**
	 * Takes in a list of objects and writes them to a given file. This method uses
	 * UTF8 character set.
	 * 
	 * @param objects
	 *            Array of items to be written to file.
	 * @param filename
	 *            filename and location of the file where list of items will be
	 *            written.
	 * @param append
	 *            boolean indicating if the file is overwritten or if the items are
	 *            written to the end of the file.
	 * @throws IOException
	 *             if an I/O error occurs writing to or creating the file
	 */
	public static void saveListToTextFile(Object[] objects, String filename, boolean append) throws IOException {
		saveListToTextFile(objects, filename, append, CHARACTER_ENCODING);
	}

	/**
	 * Takes in a list of objects and writes them to a given file.
	 * 
	 * @param objects
	 *            Array of items to be written to file.
	 * @param filename
	 *            filename and location of the file where list of items will be
	 *            written.
	 * @param append
	 *            boolean indicating if the file is overwritten or if the items are
	 *            written to the end of the file.
	 * @param characterEncoding
	 *            the Charset to be used when encoding
	 * @throws IOException
	 *             if an I/O error occurs writing to or creating the file
	 */
	public static void saveListToTextFile(Object[] objects, String filename, boolean append, Charset characterEncoding)
			throws IOException {

		Path path = Paths.get(filename);
		OpenOption option; // what is it?
		if (append)
			option = StandardOpenOption.APPEND;
		else
			option = StandardOpenOption.CREATE;

		// create list of strings
		List<String> toWrite = new ArrayList<String>(); // this con
		for (Object obj : objects)
			if (obj != null)
				toWrite.add(obj.toString());

		// write list to file
		Files.write(path, toWrite, characterEncoding, StandardOpenOption.WRITE, option);
	}

	/**
	 * Sorts a list of objects in ascending natural order using selection sort.
	 * 
	 * Precondition: Assumes that the list is not null and that the list's capacity
	 * is equal to the list's size.
	 * 
	 * @param list
	 *            A list of objects. Assumes that the list's capacity is equal to
	 *            the list's size.
	 * 
	 * @throws IllegalArgumentException
	 *             if the parameter is not full to capacity.
	 *
	 * @throws NullPointerException
	 *             if the list is null.
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })

	public static void sort(Comparable[] list) throws IllegalArgumentException, NullPointerException {
		// check if null
		if (list == null)
			throw new NullPointerException("Array can't be null , must have valeus in it ");

		// check has null param
		for (int index = 0; index < list.length; index++) {
			if (list[index] == null) {
				throw new IllegalArgumentException("Param must be full to capacity");
			} // end if
		} // end for

		// if still here, we will sort
		for (int index = 0; index < list.length; index++) {
			int minIndex=findMin(list, index);
			if (list[index].compareTo(list[minIndex]) < 0) {
				swap(list, minIndex, index); 
			} // end if
		} // end for
	}// end selection sort

	@SuppressWarnings("unchecked")
	public static int findMin(@SuppressWarnings("rawtypes") Comparable[] list, int index) {

		int minIndex = index;
		for (int index1 = index + 1; index1 < list.length; index1++) {
			if (list[index1].compareTo(list[minIndex]) < 0) {
				minIndex = index1;
			} // end if
		} // end for
		return minIndex;
	}// end method

	public static void swap(@SuppressWarnings("rawtypes") Comparable[] list, int indexActualMin, int indexItterationAtFor) {
		@SuppressWarnings("rawtypes")
		Comparable temp = list[indexActualMin];
		list[indexActualMin] = list[indexItterationAtFor];
		list[indexItterationAtFor] = temp;
	}// end method

	@SuppressWarnings({"rawtypes", "unchecked" })
	 public static Comparable[] merge (Comparable[] list1, Comparable[] list2, String duplicateFileName) {
		 
		 // Data validation
		 for (int i = 1; i < list1.length; i++) {
			 if (list1[i] == null) {
				 throw new IllegalArgumentException("The Comparable[] list1 is not full to capacity");
			 }
		 }
		 
		 for (int i = 1; i < list2.length; i++) {
			 if (list2[i] == null) {
				 throw new IllegalArgumentException("The Comparable[] list2 is not full to capacity");
			 }
		 }
		 
		 if ( (list1 == null) || (list2 == null) ) {
			 throw new NullPointerException("One of the Comparable[] lists is null");
		 }
		 
		 // If one of the lists is empty, return the other
		 if (list1.length == 0) {
			 return list2;
		 }
		 
		 if (list2.length == 0 ) {
			 return list1;
		 }
		 
		 // Create a ListArray of duplicates
		 Object[] duplicatesArray = new Object[list1.length + list2.length]; 
		 
		 Comparable[] mergedArray = (Comparable[]) Array.newInstance(
				 list1.getClass().getComponentType(), list1.length + list2.length);
			
		 int indexL1 = 0;
		 int indexL2 = 0;
		 int indexMerged = 0;
		 int indexDuplicates = 0;
			
		 // Sort the two arrays
		 while ((indexL1 < list1.length) && (indexL2 < list2.length)) {
			 		 
			 if (list1[indexL1].compareTo(list2[indexL2]) < 0) {
				 mergedArray[indexMerged] = list1[indexL1];
					
				 indexL1++;
				 indexMerged++;
			 }
			 else if (list1[indexL1].compareTo(list2[indexL2]) > 0) {
				 mergedArray[indexMerged] = list2[indexL2];
					
				 indexL2++;
				 indexMerged++;
			 }
			 else {		 
				 mergedArray[indexMerged] = list1[indexL1];
				
				 // write duplicates of list1 and list 2 to duplicatesArray
				 duplicatesArray[indexDuplicates] = list1[indexL1] + "(merged)";
				 indexDuplicates++;
				 duplicatesArray[indexDuplicates] = list2[indexL2] + "(merged)";
				 
				 indexL1++;
				 indexL2++;
				 indexMerged++;
				 indexDuplicates ++;
			}
		 }// End while
		 		
		 // Put remaining elements of one of the array in the merged array
		 if(indexL1 < list1.length) {
			 
			 for (int i = indexL1; i < list1.length; i++) {
				 mergedArray[indexMerged] = list1[i];
				 indexMerged++;
			 }
		 }
		 else if(indexL2 < list2.length){
			 for (int i = indexL2; i < list2.length; i++) {
				 mergedArray[indexMerged] = list2[i];
				 indexMerged++;
			 }	
		 }
		 
		 // Shrink mergedArray and duplicatesArray
		 Comparable[] finalMergedArray = Arrays.copyOf(mergedArray, indexMerged);
		 Object[] finalDuplicatesArray = Arrays.copyOf(duplicatesArray, indexDuplicates);
		 
		 // Write duplicates to a .txt file
		 try {
			 saveListToTextFile(finalDuplicatesArray, duplicateFileName);
		 } catch (IOException ioe) {
			 System.out.println("Duplicates were not saved");
		 }
		 
		 return finalMergedArray;
	 }// End merge method
	

	// compareTo-caseSensetive
	// with the comparator we can define other ways despite natural comparaison
	/**
	 * Sorts a list of objects in the given order.
	 * 
	 * Precondition: Assumes that the list is not null and that the list's capacity
	 * is equal to the list's size.
	 * 
	 *
	 * @param list
	 *            A list of objects. Assumes that the list's capacity is equal to
	 *            the list's size.
	 * @param sortOrder
	 *            A Comparator object that defines the sort order
	 * 
	 * 
	 * @throws IllegalArgumentException
	 *             if the parameter is * not full to capacity.
	 *
	 * @throws NullPointerException
	 *             if the list or sortOrder * are null.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void sort(Comparable[] list, Comparator sortOrder)
			throws IllegalArgumentException, NullPointerException {
		// check if null
		if (list == null)
			throw new NullPointerException("Array can't be null , must have valeus in it ");

		// check has null param
		for (int index = 0; index < list.length; index++) {
			if (list[index] == null) {
				throw new IllegalArgumentException("Param must be full to capacity");
			} // end if
		} // end for

		Arrays.sort(list, sortOrder);

	}// end method
	
	/**
	 * This method will take an array of Comparable objects and perform the binary search
	 * iteratively i.e, with loops.
	 * @param database	Array of comparable objects
	 * @param key	Comparable object
	 * @return The index of the comparable object
	 * 
	 * @author Marian Ruth Lina
	 * @version 2017-11-07
	 * 
	 */
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	public static int binarySearch(Comparable[] database, Comparable key) {
		int low = 0;
		int high = database.length -1;
		int mid = 0;
		int result = 0;
		
		while (high >= low) {
			
			mid = (high + low) /2;
			result = database[mid].compareTo(key);
			
			if (result > 0) {
				high = mid - 1;
			}else if ( result < 0) {
				low = mid + 1;
			}
			else {
				return mid;
			}
		}
		return -(low + 1);
	} // end binarySearch
	
	/**
	 * This method will take a List<Comparable> and perform the binary search
	 * recursively.
	 * 
	 * @param database List of Comparable objects	
	 * @param key	Comparable object
	 * @param low	Lowest index to perform the binary search
	 * @param high	Highest index to perform the binary search 
	 * @return The index of the comparable object
	 * 
	 * @author Marian Ruth Lina
	 * @version 2017-11-07
	 */

	public static <T extends Comparable<? super T>> int binarySearch(List<T>
	database, T key, int low, int high) {
		
		int mid = (high + low) /2;
		int result = 0;
		
		if (high >= low) {
			result = database.get(mid).compareTo(key);
			if (result > 0) {
				high = mid - 1;
				result = binarySearch (database, key, low, high);
			}else if ( result < 0) {
				low = mid + 1;
				result = binarySearch (database, key, low, high);
			}else {
				return mid;
			}
		} else {
			return -(low +1);
		}
		return result;
	}//end binarySearch

}// end class
