package util;

import util.ListUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import lib.*;

public class ListUtilitiesTest {

	public static void main(String[] args) {
		saveListToTextFileTest();
		naturalSortTest();
		testMerge();
		voterPostalCodeComTest();
		voterNameComTest();

	}// end main

	/**
	 * This will test the save list to a file
	 */

	public static void saveListToTextFileTest() {

		System.out.println(
				"--------------------------------TESTING THE saveListToTextFile METHOD--------------------------------\n\n");

		Name v1 = new Name("Lara", "Mo");
		Name v2 = new Name("Alana", "Key");
		Name v3 = null; // null for testing
		Name v4 = new Name("Dora", "Bootz");
		/// first array
		Name[] array1 = null;
		/// second array
		Name[] array2 = new Name[3];
		array2[0] = v1;
		array2[1] = v2;
		array2[2] = v4;
		try {

			saveListToTextFileTest1("CASE 1- User doesnt provide us with append or not, first overload-"
					+ " exception will be throwen===FAILED===", array1, "wontWork");
		} catch (NullPointerException npe) {

			System.err.println(npe.getMessage());
		} catch (IOException ioe) {

			System.err.println(ioe.getMessage());
		}
		try {
			saveListToTextFileTest1("CASE 2- User doesnt provide us with append or not, first overload"
					+ " will sucssefully make it in to a file===PASSED===", array2, "NamesUnsorted");
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());

		} catch (NullPointerException npe) {
			System.err.println(npe.getMessage());
		}
		try {
			saveListToTextFileTest2(
					"CASE 3- append is false, second overload-" + " exception will be throwen===FAILED===", array1,
					"wontWork", false);
		} catch (NullPointerException npe) {
			System.err.println(npe.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

		try {
			saveListToTextFileTest2("CASE 4- append is false, second overload-" + "- succesfully ===PASSED!===", array2,
					"UnsortedNamesNotAppened", false);
		} catch (NullPointerException npe) {
			System.err.println(npe.getMessage());
		} catch (IOException ioe) {

			System.err.println(ioe.getMessage());

		}
		try {

			saveListToTextFileTest2(
					"CASE 5- append is true, second overload" + " will sucssefully make it in to a file===PASSED===\n",
					array2, "NamesUnsortedAppened", true);
		} catch (NullPointerException npe) {
			System.err.println(npe.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

	}// end

	public static void saveListToTextFileTest1(String status, Object[] objects, String filename) throws IOException {
		System.out.println(status);
		ListUtilities.saveListToTextFile(objects, filename);
	}

	public static void saveListToTextFileTest2(String status, Object[] objects, String filename, boolean append)
			throws IOException {
		System.out.println(status);
		ListUtilities.saveListToTextFile(objects, filename, append);
	}

	/**
	 * This method will test the natural sorting order.
	 */

	public static void naturalSortTest() {
		System.out.println(
				"--------------------------------TESTING THE SORT METHOD(NATURAL SORT)--------------------------------\n\n");

		Name v1 = new Name("Daniella", "Jones");
		Name v2 = new Name("Alana", "Allony");
		Name v3 = null;
		Name v4 = new Name("Dora", "Bootz");
		Name v5 = new Name("Lara", "Mo");
		/// first array null
		Name[] array1 = null;
		/// second list full+null
		Name[] array2 = new Name[5];
		array2[0] = v1;
		array2[1] = v2;
		array2[2] = v3;
		array2[3] = v4;
		array2[4] = v5;
		/// third array
		Name[] array3 = new Name[4];
		array2[0] = v1;
		array2[1] = v2;
		array2[2] = v4;
		array2[3] = v5;

		// invoking the method
		try {

			sortTheVoter("CASE-1  points to null , will throw an IllegalArguemntExceptio===FAILED===", array1);
		} catch (NullPointerException npe) {

			System.err.println(npe.getMessage());
		} catch (IllegalArgumentException ioe) {

			System.err.println(ioe.getMessage());
		}
		try {
			sortTheVoter(
					"CASE-2  will sort succesfully by lastName: Expected result:Alana,Dora,Daniella,Lara=== supposed to be PASSED====",
					array2);
		} catch (NullPointerException npe) {

			System.err.println(npe.getMessage());
		} catch (IllegalArgumentException ioe) {
			System.err.println(ioe.getMessage());
		}
		try {
			sortTheVoter("CASE-3  will throw an exception(NullPointerException),since has a null value===FAILED===\n",
					array3);
		} catch (NullPointerException npe) {
			System.err.println(npe.getMessage());

		} catch (IllegalArgumentException ioe) {
			System.err.println(ioe.getMessage());
		}

	}// end

	public static void sortTheVoter(String status, Comparable[] array) {
		System.out.println(status);
		ListUtilities.sort(array);
		System.out.println("The result is" + array[0] + " " + array[1] + " " + array[2]);
	}

	/**
	 * test the overloaded sort method based on the Name's postalCode
	 */
	public static void voterPostalCodeComTest() {

		System.out.print(
				"--------------------------------TESTING THE SORT METHOD(UNATURAL SORT)--------------------------------\n\n");
		//// creating objects to put in the array of postalCodes
		PostalCode v1 = new PostalCode("H8T2B1");
		PostalCode v2 = new PostalCode("J6J3B7");
		PostalCode v3 = null;
		PostalCode v4 = new PostalCode("B7B8J3");
		//// first array
		PostalCode[] voterArray1 = null;
		//// second array
		PostalCode[] voterArray2 = new PostalCode[3];
		voterArray2[0] = v1;
		voterArray2[1] = v2;
		voterArray2[2] = v4;
		//// third array
		PostalCode[] voterArray3 = new PostalCode[5];
		voterArray3[0] = v1;
		voterArray3[1] = v3; // null
		voterArray3[2] = v2;
		voterArray3[3] = v4;

		try {

			voterPostalCodeComTest("CASE1-points to null , will throw an exception===FAILED!===", voterArray1,
					new VoterPostalCodeComparator());
		}

		catch (IllegalAccessException iae) {
			System.err.print(iae.getMessage());
		}

		catch (NullPointerException npe) {
			System.err.print(npe.getMessage());
		}

		try {
			voterPostalCodeComTest("CASE2- will sort succesfully,expected result B7B8J3,H8T2B1,J6J3B7===PASSED===",
					voterArray2, new VoterPostalCodeComparator());
		}

		catch (IllegalAccessException iae) {
			System.err.print(iae.getMessage());
		}

		catch (NullPointerException npe) {
			System.err.print(npe.getMessage());
		}
		try {

			voterPostalCodeComTest("CASE3-will throw an exception,since has a null value===FAILED!===", voterArray3,
					new VoterPostalCodeComparator());
		}

		catch (IllegalAccessException iae) {
			System.err.print(iae.getMessage());
		}

		catch (NullPointerException npe) {
			System.err.print(npe.getMessage());
		}

	}// end

	public static void voterPostalCodeComTest(String status, Comparable[] list, Comparator sortOrder) {
		System.out.println(status);
		ListUtilities.sort(list, sortOrder);
		for (int index = 0; index < list.length; index++) {
			System.out.println(list[index]);
			System.out.println("^^^^^^^^^^^^^^^^^^This is the list after we sorted it^^^^^^^^^^^^^^^^^^");
		} // end for
	}

	/**
	 * test the overloaded sort method based on the Name's name
	 */

	public static void voterNameComTest() {
		System.out.println("This method will test if the voterNameComparator is working correctly");
		//// creating objects to put in the list
		Name v1 = new Name("Lara", "Monzales");
		Name v2 = new Name("Michelle", "Kamiller");
		Name v3 = null;
		Name v4 = new Name("Dora", "Bootz");
		/// first array
		Name[] voterArray1 = null;
		/// second array
		Name[] voterArray2 = new Name[3];
		voterArray2[0] = v1;
		voterArray2[1] = v2;
		voterArray2[2] = v4;

		/// third array
		Name[] voterArray3 = new Name[4];
		voterArray2[0] = v1;
		voterArray2[1] = v2;
		voterArray2[2] = v4;
		voterArray2[3] = v3;// null

		// invoking the method
		try {
			voterNameTest("Case 1,  points to null , will throw an exception(IllegalArgumentException)", voterArray1,
					new VoterNameComparator(), false);
		} catch (IllegalAccessException iae) {
			System.err.print(iae.getMessage());
		}

		catch (NullPointerException npe) {
			System.err.print(npe.getMessage());
		}
		try {
			voterNameTest("Case 2,  will sort succesfully by lastName: Expected result:Dora,Alana,Lara", voterArray2,
					new VoterNameComparator(), true);
		} catch (IllegalAccessException iae) {
			System.err.print(iae.getMessage());
		}

		catch (NullPointerException npe) {
			System.err.print(npe.getMessage());
		}
		try {
			voterNameTest("Case 3,  will throw an exception,since has a null value(NullPointerValue)", voterArray3,
					new VoterNameComparator(), false);
		} catch (IllegalAccessException iae) {
			System.err.print(iae.getMessage());
		}

		catch (NullPointerException npe) {
			System.err.print(npe.getMessage());
		}
	}// end method

	public static void voterNameTest(String status, Comparable[] list, Comparator sortOrder, boolean check) {
		System.out.println(status);

		if (check) {
			System.out.println("^^^^^^^^^^^^^^^^^^This is the list before we sorted it^^^^^^^^^^^^^^^^^^");
			for (int index = 0; index < list.length; index++) {
				System.out.println(list[index]);
			} // end for

			System.out.println("^^^^^^^^^^^^^^^^^^This is the list after we sorted it^^^^^^^^^^^^^^^^^^");

			ListUtilities.sort(list, sortOrder);
			for (int index = 0; index < list.length; index++) {
				System.out.println(list[index]);
			} // end for

		} // end if
	}// end method

	private static void testMerge() {

		String file = "../ElectionSys/datafiles/duplicates/duplicatesTest.txt";

		Name[] nameList1 = new Name[5];
		nameList1[0] = new Name("Lara", "Mezirovsky");
		nameList1[1] = new Name("Sonya", "Rabhi");
		nameList1[2] = new Name("Tonya", "Rabhi");
		nameList1[3] = new Name("Ana", "Sarkissian");
		nameList1[4] = new Name("Zena", "Tabbal");

		Name[] nameList2 = new Name[4];
		nameList2[0] = new Name("Felipe", "Andrade");
		nameList2[1] = new Name("Shirin", "Eskandari");
		nameList2[2] = new Name("Sonya", "Rabhi");
		nameList2[3] = new Name("Zena", "Tabbal");

		Name[] nameList3 = new Name[2];
		nameList3[0] = new Name("john", "doe");

		PostalCode[] postalCodeList1 = new PostalCode[2];
		postalCodeList1[0] = new PostalCode("H1H5J8");
		postalCodeList1[1] = new PostalCode("H7N3L3");

		PostalCode[] postalCodeList2 = new PostalCode[1];
		postalCodeList2[0] = new PostalCode("H7N3L3");

		PostalCode[] postalCodeList3 = null;

		System.out.println("===== MERGE TEST CASES ===== ");
		testMerge("Case 1: two sorted Name arrays", nameList1, nameList2, file, true);
		testMerge("Case 2: two sorte PostalCode arrays", postalCodeList1, postalCodeList2, file, true);
		testMerge("Case 3: one of the arrays not full to capacity", nameList1, nameList3, file, false);

		testMerge("Case 4: one of the arrays is null", postalCodeList2, postalCodeList3, file, false);
	} // End testMerge no-parameter method

	/**
	 * Test the merge method from ListUtilities
	 * 
	 * @param testCase
	 * @param list1
	 * @param list2
	 * @param file
	 * @param success
	 * @author Sonya
	 */
	private static void testMerge(String testCase, Comparable[] list1, Comparable[] list2, String file,
			boolean success) {

		System.out.println("   " + testCase);

		try {
			Comparable[] mergedList = ListUtilities.merge(list1, list2, file);

			for (int i = 0; i < mergedList.length; i++) {
				System.out.println(mergedList[i].toString());
			}
			System.out.println("Passed Test");
		} catch (IllegalArgumentException iae) {

			System.out.println(iae.getMessage());
			if (success)
				System.out.println("  Error! Success Valid. ==== FAILED TEST ====");
			System.out.println("Passed Test");
		} catch (NullPointerException npe) {

			System.out.println(npe.getMessage());
			System.out.println("Passed Test");
		} catch (Exception e) {

			System.out.println("Unknown exception thrown");
			System.out.println("=========FAILED TEST========");
		}

		System.out.println();
	} // End testMerge with parameter method

}// end class
