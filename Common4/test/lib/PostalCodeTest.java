package lib;

import static org.junit.Assert.*;

public class PostalCodeTest {
	
	public static void main(String[] args) {
		testConstructor();
		testInRange();
		testEquals();
		testHashCode();
		testCompareTo();
	}
	
	private static void testConstructor() {
		
		System.out.println("\t\t===== CONSTRUCTOR TEST CASES ===== ");
		testConstructor("Case 1: Valid postal code j4x1l7", "j4x1l7", true);
		testConstructor("Case 2: Valid postal code with spase j4x 1l7", "j4x1l7", true);
		testConstructor("Case 3: Null postal code ", null, false);
		testConstructor("Case 4: Longer postal code ", "j4x  1l7", false);
		testConstructor("Case 5: Shorter postal code ", "j4x", false);
		testConstructor("Case 6: Incorrect alphabetic format ", "j4x127",false);
		testConstructor("Case 7: Incorrect numeric format ", "j431l7",false);
		testConstructor("Case 8: Incorrect format ", "j43@l7",false);
		testConstructor("Case 9: Incorrect first letter ", "w4x1l7",false);
		testConstructor("Case 10: Not permitted letter ", "j4q1l7",false);
		testConstructor("Case 11: Incorrect numeric format ", "j431l7",false);
	}
	
	private static void testConstructor(String testCase, String code, boolean expected) {
		
		System.out.println("   " + testCase);
		try {
			PostalCode testCode = new PostalCode(code);
			System.out.println("\tPostalCode instance is created: " + testCode);
			if(!expected)
				System.out.println("  Error! Expected Invalid. ==== FAILED TEST ====");
			System.out.println("Passed Test");
		}
		catch (IllegalArgumentException iae) {
			
			System.out.println("\t\n"+ iae.getMessage());
			if (expected)
				System.out.println("  Error! Expected Valid. ==== FAILED TEST ====");
			System.out.println("Passed Test");
		}
		
		catch (Exception e) {
			System.out.println("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() +  " "  + 	e.getMessage() + " ==== FAILED TEST ====");
			if (expected)
				System.out.print(" Expected Valid.");
		}
		System.out.println();
	}
	
	private static void testInRange() {
		
		System.out.println("\t\t===== inRange() TEST CASES ===== ");
		testInRange("Case 1: Invalid range", "j4x1l7", null, null, false);
		testInRange("Case 2: Null start and the end before given code", "j4x1l7", null, "h", false);
		testInRange("Case 3: Null end and the start before given code", "j4x1l7", "c", null, true);
		testInRange("Case 4: In range postal code with 2 char start and end.", "j4x1l7", "b5", "l7", true);
		testInRange("Case 5: Not in range postal code with 2 char start and end", "j4x1l7", "j5", "l7", false);
		testInRange("Case 6: In range postal code with different length string in start and end", "j4x1L7", "b5x", "l7", true);
		testInRange("Case 7: Start and end same as postal code", "j4x1l7", "j4x1l7", "j4x1l7", true);
		testInRange("Case 8: Not in range postal code with one character start and end", "j4x1l7", "m", "v", false);
	}
	
	private static void testInRange(String testCase, String code, String start, String end, boolean expected ) {
		
		System.out.println("   " + testCase);
		PostalCode testCode = new PostalCode(code);
		try {
			if (testCode.inRange(start, end) != expected)
				System.out.println("Error! Not Match Expected Result. ==== Faild Test ==== ");
			else 
			System.out.println("Passed Test");
		}
		catch (IllegalArgumentException iae) {
			
			System.out.println("\t"+ iae.getMessage());
			if (expected)
				System.out.println("  Error! Expected Valid. ==== FAILED TEST ====");
			System.out.println("Passed Test");
		}
		catch (Exception e) {
			System.out.println("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() +  " "  + 	e.getMessage() + " ==== FAILED TEST ====");
			
		}
	}
	
	private static void testEquals() {
		
		System.out.println("\t\t\n===== equals() TEST CASES ===== ");
		testEquals("Case 1: Null parameter", "j4x1l7", null, false);
		testEquals("Case 2: Different type parameter", "j4x1l7", "PostalCode", false);
		testEquals("Case 3: Reflectivity.", "j4x1l7", "j4x1L7", true);
		testEquals("Case 4: Different postal code ", "j4x1l7", "h4b 1l7", false);
	}
	
	private static void testEquals(String testCase, String code,Object object, boolean expected) {
		
		System.out.println("   " + testCase);
		PostalCode testCode = new PostalCode(code);
		try {
			PostalCode otherCode = new PostalCode((String)object);
			if ((testCode.equals(otherCode) != otherCode.equals(testCode))&& expected)
				System.out.println("Error! Not Match Expected Result. ==== Faild Test ==== ");
			System.out.println("Passed Test");
		}
		catch (IllegalArgumentException iae){
			
				System.out.println("Passed Test");
		}
		catch (Exception e) {
			System.out.println("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() +  " "  + 	e.getMessage() + " ==== FAILED TEST ====");
			if (expected)
				System.out.print(" Expected Valid.");
		}
		
	}
	
	private static void testHashCode() {
		
		System.out.println("\t\t===== hashCode() TEST CASES ===== ");
		testHashCode("Case 1: Null parameter", "j4x1l7", null, false);
		testHashCode("Case 2: Same postal code", "j4x1l7", "J4X1l7", true);
		testHashCode("Case 3: Different postal code", "j4x1l7", "h4b2p8", false);
	}
	
	private static void testHashCode(String testCase, String code,String other, boolean expected) {
		
		System.out.println("   " + testCase);
		try {
			PostalCode testCode = new PostalCode(code);
			PostalCode newCode = new PostalCode(other);
			assertEquals(expected, (testCode.hashCode() == newCode.hashCode()));
			System.out.println("Passed Test");
		}
		catch(IllegalArgumentException iae) {
			
			System.out.println("Passed Test");
		}
		catch (Exception e) {
			System.out.println("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() +  " "  + 	e.getMessage() + " ==== FAILED TEST ====");
			if (expected)
				System.out.print(" Expected Valid.");
		}
	}
	
	private static void testCompareTo() {
		
		System.out.println("\t\t===== compareTO() TEST CASES ===== ");
		testCompareTo("Case 1: Null parameter", "j4x1l7", null, -1);
		testCompareTo("Case 2: Same Postal Code", "j4x1l7", "j4xil7", 0);
		testCompareTo("Case 3: A smaller PostalCode", "j4x1l7", "h4b2p8", 2);
		testCompareTo("Case 3: A bigger PostalCode", "j4x1l7", "n6a1l7", -2);
	}
	
	private static void testCompareTo(String testCase, String code,String other, int expected) {
		
		System.out.println("   " + testCase);
		try {
			PostalCode testCode = new PostalCode(code);
			PostalCode newCode = new PostalCode(other);
			if ((expected > 0 && testCode.compareTo(newCode) > 0)|| (expected < 0 && testCode.compareTo(newCode) < 0))
				System.out.println("Passed Test");
		}
		catch(IllegalArgumentException iae) {
			
			System.out.println("Passed Test");
		}
		catch (Exception e) {
			System.out.println("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() +  " "  + 	e.getMessage() + " ==== FAILED TEST ====");
			
		}
	}
}
