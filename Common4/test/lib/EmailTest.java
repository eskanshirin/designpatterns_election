/**
 * 
 */
package lib;

/**
 * This class tests the methods of the Email logical class
 * @author Sonya
 * @version 26-09-2017
 */
public class EmailTest 
{
	public static void main(String[] args) 
	{
		testConstructor();
		System.out.println();
		
		testGetUserID();
		System.out.println();
		
		testGetHost();
		System.out.println();
		
		testEquals();
		System.out.println();
        
        testHashCode();
		System.out.println();
        
        testToString();
		System.out.println();
		
		testCompareTo();
        
        /* the validateEmail method is used in the constructor for data validation. 
        I chose not to create a separa testing method for it because i think it is already
         well tested in the constructor  */

	}// end main method
	
	/**
	 * This method tests the constructor of the Email class
	 * @author Sonya
	 */
	public static void testConstructor()
	{
		System.out.println("Test Email constructor");
		System.out.println();
		
		try 
		{
			System.out.println("Argument given: and empty String");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("  ");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("You have provided an empty string");
		}
		
		try 
		{
			System.out.println("Argument given: userID String that has unauthorised characters");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonyarabhi$@gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The email has some invalid characters in the userID");
		}
		
		try 
		{
			System.out.println("Argument given: host String that has unauthorised characters");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonyarabhi$@gm%ail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The email has some invalid characters in the host");
		}
		
		try 
		{
			System.out.println("Argument given: String that has no @ sign");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonya.rabhilocal.gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The email has no @ sign");
		}
		
		try 
		{
			System.out.println("Argument given: userID that starts with a period");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email(".sonya.rabhi@gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The email cannot start with a period");
		}
		
		try 
		{
			System.out.println("Argument given: userID that ends with a period");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonya.rabhi.@gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The user ID of the email cannot end with a period");
		}
		
		try 
		{
			System.out.println("Argument given: email that has two consecutive periods");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonya..rabhi.@localhost.gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The email cannot contain two consecutive periods");
		}
		
		try 
		{
			System.out.println("Argument given: user ID that has no characters");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("@localhost.gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The userID cannot have less than 1 character");
		}
		
		try 
		{
			System.out.println("Argument given: user ID that exceeds 32 characters");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonya.rabhi.sdflgdljhlasdkjfhalskdjfhaldskjhfaksjhfflkgjzdfklgjsdlkfgjszdlkfgjdflkjg@localhost.gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The user ID cannot exceed 32 characters");
		}
		
		try 
		{
			System.out.println("Argument given: host that has no characters");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonyarabhi@");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The host cannot have less than 1 character");
		}
		
		try 
		{
			System.out.println("Argument given: host that exceeds 32 characters");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonya.rabhi@localhost.gmailllllllllllllllllllllllllllllllllllllllllllllllllllll.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The host cannot exceed 32 characters");
		}
		
		try 
		{
			System.out.println("Argument given: host that starts with a hyphen");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonya.rabhi.@localhost.-gmail.com");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The name of the host cannot start with a hyphen");
		}
		
		try 
		{
			System.out.println("Argument given: host that ends with a hyphen");
			System.out.println("Expected result: exception caught");
			System.out.println("Actual result: ");
			System.out.println();
			
			Email emailAddress = new Email("sonya.rabhi.@localhost.gmail.com-");
			System.out.println(emailAddress.getAddress());
			
		} catch (IllegalArgumentException e) 
		{
			System.out.println("The name of the host cannot end with a hyphen");
		}
				
		System.out.println("Argument given: valid email");
		System.out.println("Expected result: email shown");
		System.out.println("Actual result: ");
			
		Email emailAddress = new Email("sonya_rabhi@localhost");
		System.out.println(emailAddress.getAddress());
		
	}// end TestConstructor method 
	
	/**
	 * This method tests the getUserID method
	 * @author Sonya
	 */
	public static void testGetUserID()
	{
		System.out.println("Test getUserID");
		System.out.println();
		
		System.out.println("Expected result: the user id");
		System.out.println("Actual result: ");
			
		Email emailAddress = new Email("sonyarabhi@gmail.com");
		System.out.println(emailAddress.getUserID());
	}// end testGetUserID method
	
	/**
	 * This method tests the getHost method
	 * @author Sonya
	 */
	public static void testGetHost()
	{
		System.out.println("Test getHost");
		System.out.println();
		
		System.out.println("Expected result: the user host");
		System.out.println("Actual result: ");
			
		Email emailAddress = new Email("sonyarabhi@gmail.com");
		System.out.println(emailAddress.getHost());
	}// end testGetHost method
	
	/**
	 * This method tests the overriden equals method
	 * @author Sonya
	 */
	public static void testEquals()
	{
		System.out.println("Test equals method");
		System.out.println();
		
		System.out.println("Argument given: a null object");
		System.out.println("Expected result: false");
		System.out.println("Actual result: ");
		System.out.println();
			
		Email emailAddress = new Email("sonyarabhi@gmail.com");
		Email nullEmailAddress = null;
		System.out.println(emailAddress.equals(nullEmailAddress));
		
		System.out.println("Argument given: the same object that has been instantiated");
		System.out.println("Expected result: true");
		System.out.println("Actual result: ");
		System.out.println();
			
		System.out.println(emailAddress.equals(emailAddress));
		
		System.out.println("Argument given: another instance of class, but same email");
		System.out.println("Expected result: true");
		System.out.println("Actual result: ");
		System.out.println();
			
		Email sameEmailAddress = new Email("sonyarabhi@gmail.com");
		System.out.println(emailAddress.equals(sameEmailAddress));
		
	}// end testEquals method
    
    /**
     * This method tests the overriden hashCode method
     * @author Sonya
     */
    public static void testHashCode()
    {
        System.out.println("Test hashCode method");
		System.out.println();
		
		System.out.println("Creating two instances of Email that have the same email address");
		System.out.println("Expected result: the hash code of these two objects will be the same");
		System.out.println("Actual result: ");
		System.out.println();
        
        Email emailAddress = new Email("sonyarabhi@gmail.com");
        Email sameEmailAddress = new Email("sonyarabhi@gmail.com");
        System.out.println("Object1's hash code: " + emailAddress.hashCode() + " Object2's hash code: " + sameEmailAddress.hashCode());
    }// end testHashCode method
    
    /**
     * This method tests the overriden toString method
     * @author Sonya
     */
    public static void testToString()
    {
        System.out.println("Test toString method");
		System.out.println();
		
		System.out.println("Creating two instances of Email that have the same email address");
		System.out.println("Expected result: the return of toString of these two objects will be the same");
		System.out.println("Actual result: ");
		System.out.println();
        
        Email emailAddress = new Email("sonyarabhi@gmail.com");
        Email sameEmailAddress = new Email("sonyarabhi@gmail.com");
        System.out.println("Object1's toString: " + emailAddress.toString() + " Object2's toString: " + sameEmailAddress.toString());
    }// end testToString method
    
    public static void testCompareTo()
    {
        System.out.println("Test compareTo method");
		System.out.println();
		
		System.out.println("Creating two instances of Email that have the same email address");
		System.out.println("Expected result: the return of compareTo will be 0");
		System.out.println("Actual result: ");
		System.out.println();
        
        Email emailAddress = new Email("sonyarabhi@gmail.com");
        Email sameEmailAddress = new Email("sonyarabhi@gmail.com");
        System.out.println("Result for 2 equal addresses: " + emailAddress.compareTo(sameEmailAddress));
        
        System.out.println();
		
		System.out.println("Compare two instances of Email where the second email address is greater");
		System.out.println("Expected result: negative integer");
		System.out.println("Actual result: ");
		System.out.println();
        
        Email greaterEmailAddress = new Email("tonyarabhi@gmail.com");
        System.out.println("Result when the second address is greater :" + emailAddress.compareTo(greaterEmailAddress));
        
        System.out.println();
		
		System.out.println("Compare two instances of Email where the first email address is greater");
		System.out.println("Expected result: positive integer");
		System.out.println("Actual result: ");
		System.out.println();

        System.out.println("Result when the first address is greater: " + greaterEmailAddress.compareTo(emailAddress));
    }// end testToString method

}// end EmailTest class 
