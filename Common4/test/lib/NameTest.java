/**
 * 
 */
package lib;

/**
 * test class for Name class
 * @author shirin
 * @version 2017-09-28
 */
public class NameTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		    testTwoParameterConstructor();
		    testGetFirstName();
		    testGetLastName();
		    testGetFullName();
		    testEquals();
		    testHashCode();
		    testCompareTo();
		    testToString();
		   
		   
		  }
		    private static void testTwoParameterConstructor()
		    {
		      System.out.println("\nTesting the two parametere constructer");
		      testTwoParameterConstructor("Case1 - valid data,Shirin*Eskandari","Shirin","Eskandari",true);
		      testTwoParameterConstructor("Case2 - valid data(space accepted),Sh rin*Eskandari","Sh rin","Eskandari",true);
		      testTwoParameterConstructor("Case3 - valid data(hyphen accepted),Sh-rin*Eskandari","Sh-rin","Eskandari",true);
		      testTwoParameterConstructor("Case4 - valid data(' accepted),Sh'rin*Eskandari","Sh'rin","Eskandari",true);
		      testTwoParameterConstructor("Case5 - invalid data(name less than 2 letter),S*Eskandari","S","Eskandari",false);
		      testTwoParameterConstructor("Case6 - invalid data(lastName less than 2 letter),Shirin*E","Shirin","E",false);
		      testTwoParameterConstructor("Case7 - invalid data(null firstName),null*Eskandari",null,"Eskandari",false);
		      testTwoParameterConstructor("Case8 - invalid data(null lastName),Shirin*null","Shirin",null,false);
		      testTwoParameterConstructor("Case9 - invalid data(empty firstName),empty*Eskandari","","Eskandari",false);
		      testTwoParameterConstructor("Case10 - invalid data(empty lastName),Shirin*empty","Shirin","",false);
		      testTwoParameterConstructor("Case11 - valid data(all spaces name ),   *Eskandari","   ","Eskandari",false);
		      testTwoParameterConstructor("Case 12 - valid data(all spaces lastName),Shirin*   ","Shirin","    ",false);
		    }
		    private static void testTwoParameterConstructor(String testCase, String firstName, String lastName, boolean expectValid)
		    {
		      System.out.println("   "+testCase);
		      
		      try{
		        Name name = new Name (firstName,lastName);
		           System.out.println("   "+name);
		           
		           if (!expectValid)
		                System.out.println(" Error! EXPECTED INVALID. == FAILED TEST=="  );
		              System.out.println("\n");
		      }
		      catch(IllegalArgumentException iae){
		        System.out.print("\t"+ iae.getMessage());
		   if (expectValid)
		    System.out.print("  Error! Expected Valid. ==== FAILED TEST ====");
		  }
		  catch (Exception e) {
		   System.out.print("\tUNEXPECTED EXCEPTION TYPE! " + e.getClass() + " " +
		     e.getMessage() + " ==== FAILED TEST ====");
		   if (expectValid)
		    System.out.print(" Expected Valid.");
		  }

		  System.out.println("\n");
		 }
		    
		    private static void testGetFirstName()
		    {
		      System.out.println("\n\nTesting the getFirstName method.");
		   testGetFirstName("Case 1: first name without leading/trailing space (Shirin)", "Shirin", "Shirin");
		   testGetFirstName("Case 2: first name with leading/trailing spaces (   shirin   )", "   shirin   ", "shirin");
		    }
		    
		    private static void testGetFirstName(String testCase, String firstName,String expectedFirstName)
		    {
		       System.out.println("   "+testCase);
		      
		        Name name = new Name (firstName,"Eskandari");
		           System.out.println("   "+name);
		           
		           System.out.print("\tName instance was created: " + name);

		  if (!name.getFirstName().equals(expectedFirstName))
		   System.out.print("  Error! Expected Invalid. ==== FAILED TEST ====");

		  System.out.println("\n");
		 }
		    
		    private static void testGetLastName() {
		   System.out.println("\n\nTesting the getLastName method.");
		   testGetLastName("Case 1: last name without leading/trailing space (Eskandari)", "Eskandari", "Eskandari");
		   testGetLastName("Case 2: last name with leading/trailing spaces (   Eskandari  )", "  Eskandari   ", "Eskandari");
		  }
		 
		  private static void testGetLastName(String testCase, String lastName, String expectedLastName) {
		   System.out.println("    " + testCase);
		   Name last = new Name("Shirin", lastName);
		   System.out.println("\tThe Name instace was created: " +last );
		 
		   if (!last.getLastName().equals(expectedLastName)) {
		    System.out.print("  Error! Expected Invalid. ==== FAILED TEST ====");
		   }
		   System.out.println("\n");
		  }//testGetLastName

		 private static void testGetFullName() {
		   System.out.println("\n\nTesting the getFullName method.");
		   testGetFullName("Case 1: full name without leading/trailing space (Shirin*Eskandari)", "Shirin", "Eskandari",
		     "Shirin Eskandari");
		   testGetFullName(
		     "Case 2: full name with leading/trailing spaces for first name and last name ( Shirin   Eskandari  )",
		     "  Shirin  ", "  Eskandari   ", "Shirin Eskandari");
		  }//end testGetFullName
		 
		  private static void testGetFullName(String testCase, String firstName, String lastName, String expectedFullName) {
		   System.out.println("    " + testCase);
		   Name full = new Name(firstName, lastName);
		   System.out.println("\tThe Name instace was created: " + full);
		 
		   if (!full.getFullName().equals(expectedFullName)) {
		    System.out.print("  Error! Expected Invalid. ==== FAILED TEST ====");
		  }
		   System.out.println("\n");
		  }
		  public static void testEquals()
			{
				System.out.println("\n Testing the  equals method");
				
				
				
				try { Name name = new Name (null,"Eskandari");
		           System.out.println("   "+name);
					
				System.out.println(name.equals(name));
				}
			
				catch (Exception e){
					System.out.println("case 1- Argument is  a null object expected result is false");
				}
				
				
					try { Name name = new Name ("shirin","Eskandari");
			       
				System.out.println("Case2- Argument is  the same object that has been instantiated true");
				System.out.println(name.equals(name));
			    System.out.println("   "+name);
					}
					catch (Exception e){
						System.out.println("case 2 is correct");
					}
			
					try { Name name = new Name ("shirin","Eskandari");
					Name name6 = new Name ("shirin","Eskandari");
				       
					System.out.println("Case3- Argument is  same object that has been instantiated true");
					System.out.println(name.equals(name6));
				    System.out.println("   "+name);
						}
						catch (Exception e){
							System.out.println("case 3 is incorrect false");
						}
				
			}//testEquals
			
		   public static void testHashCode()
		    {
		        System.out.println("\n Testing the  hashCode method");
				
				
		      try {  
		    	  Name name = new Name ("shirin","Eskandari");
					Name name6 = new Name ("shirin","Eskandari");
				       
					System.out.println("Case1- instantiated two Name with the same name expected code will be the same true");
					System.out.println("Object1's hash code: " + name.hashCode() + " Object2's hash code: " + name6.hashCode());
		      }
		      catch (Exception e){
					System.out.println("case 1 is incorrect FALSE");
				}
		      try {  
		    	  Name name = new Name ("shirin","Eskandari");
					Name name6 = new Name ("shadi","Eskandari");
				       
					System.out.println("Case2- instantiated two Name with the different name expected code will be false");
					System.out.println("Object1's hash code: " + name.hashCode() + " Object2's hash code: " + name6.hashCode());
		      }
		      catch (Exception e){
					System.out.println("case 2 is incorrect FALSE");
				}
		    }//testHashCode
		   
		   public static  void testToString() {
			   Name last2 = new Name("Shirin", "eskandari");
	    		System.out.print("\n\nTesting for toString, should print shirin*eskandari and the result is = "
	    							 +last2.toString());
	    	}//end toStringTest
		   public static void testCompareTo() {
			   System.out.println("\n\nTesting the compareTo method.");
			   testCompareTo("Case 1: will return 0 , they are equal","shirin",0);
			   testCompareTo("\nCase 2: will return negative , they are not equal false","shirin",1);
			   testCompareTo("\nCase 3: will return positive , they are notequal false","shirin",-1);
	    	}
	    	public  static void  testCompareTo(String testCase,String name,int expected) {
	    		System.out.println("    " + testCase);
	 		   Name last = new Name("Shirin", "eskandari");
	 		
			   Name last2 = new Name("Shirin", "eskandari");
			   Name last3 = new Name("Shirrin", "eskandari");
			   Name last4 = new Name("Shiin", "eskandari");
	    		if(expected==0) {
	    		System.out.print("expected result=0 and  the result = " + last.compareTo(last2)); 
	    		}
	    		
	    		if(expected==1) {	
	        	System.out.print("expected result=negative  and the result =  " + last.compareTo(last3)); 
	    		}
	        	if(expected==-1) {	
	            	System.out.print("expected result= positive and the result =  " + last.compareTo(last4)); 
	        	}
	    	}//end compareTo()
		 
		}//end test class

		      



